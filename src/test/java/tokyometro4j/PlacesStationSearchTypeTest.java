package tokyometro4j;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import tokyometro4j.common.RailWay;
import tokyometro4j.entity.Station;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PlacesStationSearchTypeTest {

    private PlacesStationSearchType sut;

    private static final String consumerKey = "0e1f4588c5060ef7eaf6d3279a503693f109f0af43af3fd2300230e390642bc2";

    @Rule
    public ExpectedException e = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        sut = new TokyoMetro(consumerKey).places().station();
    }

    @Test
    public void testLat() throws Exception {
        sut.lat(35.705649);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("lat"));
        assertThat(sut.params.get(0).getValue(), is("35.705649"));
    }

    @Test
    public void testLon() throws Exception {
        sut.lon(139.751891);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("lon"));
        assertThat(sut.params.get(0).getValue(), is("139.751891"));
    }

    @Test
    public void testRadius() throws Exception {
        sut.radius(100.0);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("radius"));
        assertThat(sut.params.get(0).getValue(), is("100.0"));
    }

    @Test
    public void testExecute_パラメータを何も指定しない場合_IOException発生() throws Exception {
        e.expect(IOException.class);
        e.expectMessage("lat, lon, radius is required parameters.");

        sut.execute();
    }

    @Test
    public void testExecute_半径を指定しない場合_IOException発生() throws Exception {
        e.expect(IOException.class);
        e.expectMessage("lat, lon, radius is required parameters.");

        sut.lat(35.705649).lon(139.751891).execute();
    }

    @Test
    public void testExecute_東京ドーム半径1km以内_取得可能() throws Exception {
        // 東京ドーム 35.705649, 139.751891
        // 実際にAPIを呼んでいます

        List<Station> result = sut.lat(35.705649).lon(139.751891).radius(1000).execute();

        assertThat(result, is(not(Collections.EMPTY_LIST)));
        assertThat(result.size(), is(6));
        assertThat(result.get(0).title, is("後楽園"));
        assertThat(result.get(0).railway, is(RailWay.MARUNOUCHI));
        assertThat(result.get(0).stationCode, is("M22"));
        assertThat(result.get(1).title, is("後楽園"));
        assertThat(result.get(1).railway, is(RailWay.NAMBOKU));
        assertThat(result.get(1).stationCode, is("N11"));
        assertThat(result.get(2).title, is("飯田橋"));
        assertThat(result.get(2).railway, is(RailWay.TOZAI));
        assertThat(result.get(2).stationCode, is("T06"));
        assertThat(result.get(3).title, is("本郷三丁目"));
        assertThat(result.get(3).railway, is(RailWay.MARUNOUCHI));
        assertThat(result.get(3).stationCode, is("M21"));
        assertThat(result.get(4).title, is("飯田橋"));
        assertThat(result.get(4).railway, is(RailWay.NAMBOKU));
        assertThat(result.get(4).stationCode, is("N10"));
        assertThat(result.get(5).title, is("飯田橋"));
        assertThat(result.get(5).railway, is(RailWay.YURAKUCHO));
        assertThat(result.get(5).stationCode, is("Y13"));
    }
}