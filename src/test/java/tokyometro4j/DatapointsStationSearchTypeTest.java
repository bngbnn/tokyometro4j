package tokyometro4j;

import org.junit.Before;
import org.junit.Test;
import tokyometro4j.common.Operator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DatapointsStationSearchTypeTest {

    private DatapointsStationSearchType sut;

    private static final String consumerKey = "0e1f4588c5060ef7eaf6d3279a503693f109f0af43af3fd2300230e390642bc2";

    @Before
    public void setUp() throws Exception {
        sut = new TokyoMetro(consumerKey).datapoints().station();
    }

    @Test
    public void testId() throws Exception {
        sut.id("urn:ucode:_00001C000000000000010000030C46CD");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("@id"));
        assertThat(sut.params.get(0).getValue(), is("urn:ucode:_00001C000000000000010000030C46CD"));
    }

    @Test
    public void testSameAs() throws Exception {
        sut.sameAs("odpt.Station:TokyoMetro.Hibiya.Ueno");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("owl:sameAs"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Station:TokyoMetro.Hibiya.Ueno"));
    }

    @Test
    public void testTitle() throws Exception {
        sut.title("上野");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("dc:title"));
        assertThat(sut.params.get(0).getValue(), is("上野"));
    }

    @Test
    public void testOperator() throws Exception {
        sut.operator(Operator.TOKYOMETRO);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:operator"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Operator:TokyoMetro"));
    }

    @Test
    public void testStationCode() throws Exception {
        sut.stationCode("H17");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:stationCode"));
        assertThat(sut.params.get(0).getValue(), is("H17"));
    }
}