package tokyometro4j;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailWay;
import tokyometro4j.entity.PassengerSurvey;
import tokyometro4j.entity.RailwayFare;
import tokyometro4j.entity.Station;
import tokyometro4j.entity.StationFacility;
import tokyometro4j.entity.StationTimetable;
import tokyometro4j.entity.Train;
import tokyometro4j.entity.TrainInformation;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class DatapointsSearchTest {

    DatapointsSearch sut;

    @Before
    public void setUp() {
        sut = new TokyoMetro().datapoints();
    }

    @Test
    public void testId_stationTimetable() throws Exception {
        StationTimetable response = (StationTimetable) sut.id("urn:ucode:_00001C000000000000010000030C3D46");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030C3D46"));
        assertThat(response.getType(), is("odpt:StationTimetable"));
        assertThat(response.getSameAs(), is("odpt.StationTimetable:TokyoMetro.Ginza.AoyamaItchome.Shibuya"));
    }

    @Ignore("APIの不具合？結果が取得できない。")
    @Test
    public void testId_trainInformation() throws Exception {
        TrainInformation response = (TrainInformation) sut.id("urn:ucode:_00001C000000000000010000030C3BE5");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030C3BE5"));
        assertThat(response.getType(), is("odpt:TrainInformation"));
        assertThat(response.getRailway(), is(RailWay.HANZOMON));
    }

    @Ignore("APIで取得したucodeで再検索をかけると何故か運賃が取れる。。。")
    @Test
    public void testId_train() throws Exception {
        Train response = (Train) sut.id("urn:ucode:_00001C000000000000010000030CA198");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030CA198"));
        assertThat(response.getType(), is("odpt:Train"));
        assertThat(response.getSameAs(), is("odpt.Train:TokyoMetro.Marunouchi.B1737"));
        assertThat(response.getFromStation(), is(tokyometro4j.common.Station.MARUNOUCHI_OGIKUBO));
        assertThat(response.getToStation(), is(tokyometro4j.common.Station.MARUNOUCHI_IKEBUKURO));
    }

    @Test
    public void testId_passengerSurvey() throws Exception {
        PassengerSurvey response = (PassengerSurvey) sut.id("urn:ucode:_00001C000000000000010000030C44FD");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030C44FD"));
        assertThat(response.getType(), is("odpt:PassengerSurvey"));
        assertThat(response.getSameAs(), is("odpt.PassengerSurvey:TokyoMetro.Ikebukuro.2013"));
    }

    @Test
    public void testId_station() throws Exception {
        Station response = (Station) sut.id("urn:ucode:_00001C000000000000010000030C46B8");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030C46B8"));
        //TODO TokyoMetroのバグが治ったらコメント解除する
        //        assertThat(response.getType(), is("odpt:Station"));
        assertThat(response.getSameAs(), is("odpt.Station:TokyoMetro.Hibiya.Akihabara"));
        assertThat(response.title, is("秋葉原"));
        assertThat(response.connectingRailways.size(), is(4));
    }

    @Test
    public void testId_stationFacility() throws Exception {
        StationFacility response = (StationFacility) sut.id("urn:ucode:_00001C000000000000010000030C478A");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030C478A"));
        assertThat(response.getType(), is("odpt:StationFacility"));
        assertThat(response.getSameAs(), is("odpt.StationFacility:TokyoMetro.KokkaiGijidomae"));
    }

    @Test
    public void testId_railwayFare() throws Exception {
        RailwayFare response = (RailwayFare) sut.id("urn:ucode:_00001C000000000000010000030CDD7D");
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030CDD7D"));
        assertThat(response.getType(), is("odpt:RailwayFare"));
        assertThat(response.getSameAs(), is("odpt.RailwayFare:TokyoMetro.Fukutoshin.Hikawadai.TokyoMetro.Fukutoshin.Heiwadai"));
        assertThat(response.getDate(), is("2014-08-29T16:03:36+09:00"));
        assertThat(response.getOperator(), is(Operator.TOKYOMETRO));
    }

    /*
        STATION_FACILITY("odpt:StationFacility"),
        RAILWAY("odpt:Railway"),
        RAILWAY_FARE("odpt:RailwayFare"),;
     */

}