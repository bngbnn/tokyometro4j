package tokyometro4j;

import org.junit.Before;
import org.junit.Test;
import tokyometro4j.common.Operator;
import tokyometro4j.entity.Railway;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class DatapointsRailwaySearchTypeTest {

    private DatapointsRailwaySearchType sut;

    private static final String consumerKey = "0e1f4588c5060ef7eaf6d3279a503693f109f0af43af3fd2300230e390642bc2";

    @Before
    public void setUp() throws Exception {
        sut = new TokyoMetro(consumerKey).datapoints().railway();
    }

    @Test
    public void testId() throws Exception {
        sut.id("urn:ucode:_00001C000000000000010000030C46CD");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("@id"));
        assertThat(sut.params.get(0).getValue(), is("urn:ucode:_00001C000000000000010000030C46CD"));
    }

    @Test
    public void testSameAs() throws Exception {
        sut.sameAs("odpt.Station:TokyoMetro.Hibiya.Ueno");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("owl:sameAs"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Station:TokyoMetro.Hibiya.Ueno"));
    }

    @Test
    public void testTitle() throws Exception {
        sut.title("上野");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("dc:title"));
        assertThat(sut.params.get(0).getValue(), is("上野"));
    }

    @Test
    public void testOperator() throws Exception {
        sut.operator(Operator.TOKYOMETRO);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:operator"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Operator:TokyoMetro"));
    }

    @Test
    public void testLineCode() throws Exception {
        sut.lineCode("T");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:lineCode"));
        assertThat(sut.params.get(0).getValue(), is("T"));
    }

    @Test
    public void testExecute() throws Exception {
        List<Railway> result = sut.lineCode("T").lineCode("H").execute();

        assertThat(result, is(not(Collections.EMPTY_LIST)));
        assertThat(result.size(), is(1));
        Railway railway = result.get(0);
        assertThat(railway, is(not(nullValue())));
        assertThat(railway.getContext(), is("http://vocab.tokyometroapp.jp/context_odpt_Railway.jsonld"));
        assertThat(railway.getId(), is("urn:ucode:_00001C000000000000010000030C46AE"));
        assertThat(railway.getType(), is("odpt:Railway"));
        assertThat(railway.getSameAs(), is("odpt.Railway:TokyoMetro.Hibiya"));
        assertThat(railway.getTitle(), is("日比谷"));
        assertThat(railway.getStationOrders(), is(not(nullValue())));
        assertThat(railway.getStationOrders().size(), is(21));
        assertThat(railway.getTravelTimes(), is(not(nullValue())));
        assertThat(railway.getTravelTimes().size(), is(40));
        assertThat(railway.getLineCode(), is("H"));
        assertThat(railway.getWomenOnlyCars(), is(not(nullValue())));
        assertThat(railway.getWomenOnlyCars().size(), is(1));
        assertThat(railway.getRegion(), is("https://api.tokyometroapp.jp/api/v2/places/urn:ucode:_00001C000000000000010000030C46AE.geojson"));
        assertThat(railway.getDate(), is("2014-09-29T19:42:45+09:00"));
        assertThat(railway.getOperator(), is(Operator.TOKYOMETRO));
    }
}