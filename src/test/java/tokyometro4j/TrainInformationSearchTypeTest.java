package tokyometro4j;

import org.junit.Before;
import org.junit.Test;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailWay;
import tokyometro4j.entity.TrainInformation;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class TrainInformationSearchTypeTest {

    TrainInformationSearchType sut;

    @Before
    public void setUp() {
        sut = new TokyoMetro("0e1f4588c5060ef7eaf6d3279a503693f109f0af43af3fd2300230e390642bc2").datapoints().trainInformation();
    }

    @Test
    public void testOperator() {
        sut.operator(Operator.TOKYOMETRO);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:operator"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Operator:TokyoMetro"));
    }

    @Test
    public void testRailway() throws Exception {
        sut.railway(RailWay.CHIYODA);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:railway"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Railway:TokyoMetro.Chiyoda"));
    }

    @Test
    public void testTraininformationStatus() {
        sut.trainInformationStatus("testValue");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:trainInformationStatus"));
        assertThat(sut.params.get(0).getValue(), is("testValue"));
    }

    @Test
    public void testTraininformationText() {
        sut.trainInformationText("testText");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:trainInformationText"));
        assertThat(sut.params.get(0).getValue(), is("testText"));
    }

    @Test
    public void testExecute() throws Exception {
        List<TrainInformation> result = sut.railway(RailWay.CHIYODA).execute();
        assertThat(result.size(), is(1));
        assertThat(result.get(0).getRailway(), is(RailWay.CHIYODA));
    }

}