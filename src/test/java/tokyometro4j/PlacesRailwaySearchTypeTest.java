package tokyometro4j;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import tokyometro4j.entity.Railway;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PlacesRailwaySearchTypeTest {

    PlacesRailwaySearchType sut;

    private static final String consumerKey = "0e1f4588c5060ef7eaf6d3279a503693f109f0af43af3fd2300230e390642bc2";

    @Rule
    public ExpectedException e = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        sut = new TokyoMetro(consumerKey).places().railway();
    }

    @Test
    public void testLat() throws Exception {
        sut.lat(35.705649);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("lat"));
        assertThat(sut.params.get(0).getValue(), is("35.705649"));
    }

    @Test
    public void testLon() throws Exception {
        sut.lon(139.751891);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("lon"));
        assertThat(sut.params.get(0).getValue(), is("139.751891"));
    }

    @Test
    public void testRadius() throws Exception {
        sut.radius(100.0);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("radius"));
        assertThat(sut.params.get(0).getValue(), is("100.0"));
    }

    @Test
    public void testExecute_パラメータを何も指定しない場合_IOException発生() throws Exception {
        e.expect(IOException.class);
        e.expectMessage("lat, lon, radius is required parameters.");

        sut.execute();
    }

    @Test
    public void testExecute_半径を指定しない場合_IOException発生() throws Exception {
        e.expect(IOException.class);
        e.expectMessage("lat, lon, radius is required parameters.");

        sut.lat(35.705649).lon(139.751891).execute();
    }

    @Test
    public void testExecute_東京ドーム半径1km以内_取得可能() throws Exception {
        // 東京ドーム 35.705649, 139.751891
        // 実際にAPIを呼んでいます

        List<Railway> result = sut.lat(35.705649).lon(139.751891).radius(1000).execute();

        assertThat(result, is(not(Collections.EMPTY_LIST)));
        assertThat(result.size(), is(4));
        assertThat(result.get(0).getTitle(), is("南北"));
        assertThat(result.get(0).getSameAs(), is("odpt.Railway:TokyoMetro.Namboku"));
        assertThat(result.get(0).getLineCode(), is("N"));
        assertThat(result.get(0).getWomenOnlyCars(), is(not(nullValue())));
        assertThat(result.get(0).getWomenOnlyCars(), is(Collections.EMPTY_LIST));
        assertThat(result.get(1).getTitle(), is("丸ノ内"));
        assertThat(result.get(1).getSameAs(), is("odpt.Railway:TokyoMetro.Marunouchi"));
        assertThat(result.get(1).getLineCode(), is("M"));
        assertThat(result.get(1).getWomenOnlyCars(), is(not(nullValue())));
        assertThat(result.get(1).getWomenOnlyCars(), is(Collections.EMPTY_LIST));
        assertThat(result.get(2).getTitle(), is("東西"));
        assertThat(result.get(2).getSameAs(), is("odpt.Railway:TokyoMetro.Tozai"));
        assertThat(result.get(2).getLineCode(), is("T"));
        assertThat(result.get(2).getWomenOnlyCars(), is(not(nullValue())));
        assertThat(result.get(2).getWomenOnlyCars().size(), is(2));
        assertThat(result.get(3).getTitle(), is("有楽町"));
        assertThat(result.get(3).getSameAs(), is("odpt.Railway:TokyoMetro.Yurakucho"));
        assertThat(result.get(3).getLineCode(), is("Y"));
        assertThat(result.get(3).getWomenOnlyCars(), is(not(nullValue())));
        assertThat(result.get(3).getWomenOnlyCars().size(), is(2));
    }

}