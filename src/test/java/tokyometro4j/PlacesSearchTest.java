package tokyometro4j;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import tokyometro4j.entity.Station;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PlacesSearchTest {

    PlacesSearch sut;

    @Before
    public void setUp() {
        sut = new TokyoMetro().places();
    }

    @Test
    public void testId_station() throws Exception {
        Station response = (Station) sut.id("urn:ucode:_00001C000000000000010000030C46B8");
        assertThat(response.id, is("urn:ucode:_00001C000000000000010000030C46B8"));
        assertThat(response.type, is("odpt:Station"));
        assertThat(response.sameAs, is("odpt.Station:TokyoMetro.Hibiya.Akihabara"));
        assertThat(response.title, is("秋葉原"));
        assertThat(response.connectingRailways.size(), is(4));
    }


    @Ignore("Railwayが実装されたら追加する")
    @Test
    public void testId_railway() throws Exception {

    }

}