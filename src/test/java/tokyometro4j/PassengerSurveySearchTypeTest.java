package tokyometro4j;

import org.junit.Before;
import org.junit.Test;
import tokyometro4j.common.Operator;
import tokyometro4j.entity.PassengerSurvey;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PassengerSurveySearchTypeTest {

    PassengerSurveySearchType sut;

    @Before
    public void setUp() {
        sut = new TokyoMetro().datapoints().passengerSurvey();
    }

    @Test
    public void testSameAs() throws Exception {
        List<PassengerSurvey> result = sut.sameAs("odpt.PassengerSurvey:TokyoMetro.Iidabashi.2013").execute();
        assertThat(result.size(), is(1));
    }

    @Test
    public void testOperator() throws Exception {
        List<PassengerSurvey> result = sut.operator(Operator.TOKYOMETRO).execute();
        assertThat(result.size(), not(is(0)));
        for (PassengerSurvey passengerSurvey : result) {
            assertThat(passengerSurvey.getOperator(), is(Operator.TOKYOMETRO));
        }
    }

    @Test
    public void testSurveyYear() throws Exception {
        List<PassengerSurvey> result = sut.surveyYear(2013).execute();
        assertThat(result.size(), not(is(0)));
        for (PassengerSurvey passengerSurvey : result) {
            assertThat(passengerSurvey.getSurveyYear(), is(2013));
        }
    }

    @Test
    public void testExecute() throws Exception {
        List<PassengerSurvey> result = sut.sameAs("odpt.PassengerSurvey:TokyoMetro.Iidabashi.2013").execute();
        assertThat(result.size(), is(1));
        System.out.println(result.get(0));
    }
}