package tokyometro4j;

import org.junit.Test;
import tokyometro4j.common.Station;

public class DatapointsStationTimetableSearchTypeTest {

    @Test
    public void testExecute() throws Exception {
        new TokyoMetro().datapoints().stationTimetable().station(Station.CHIYODA_OMOTE_SANDO).execute();
    }
}