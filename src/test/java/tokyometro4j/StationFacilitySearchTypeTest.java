package tokyometro4j;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import tokyometro4j.common.RailDirection;
import tokyometro4j.common.RailWay;
import tokyometro4j.entity.BarrierfreeFacility;
import tokyometro4j.entity.PlatformInformation;
import tokyometro4j.entity.StationFacility;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class StationFacilitySearchTypeTest extends TestCase {

    StationFacilitySearchType sut;

    @Before
    public void setUp() {
        sut = new TokyoMetro().datapoints().stationFacility();
    }

    @Test
    public void testSameAs() {
        sut.sameAs("odpt.StationFacility:TokyoMetro.KokkaiGijidomae");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("owl:sameAs"));
        assertThat(sut.params.get(0).getValue(), is("odpt.StationFacility:TokyoMetro.KokkaiGijidomae"));
    }

    @Test
    public void testExecute() throws Exception {
        List<StationFacility> result = sut.sameAs("odpt.StationFacility:TokyoMetro.Iidabashi").execute();
        assertThat(result.size(), is(1));
        assertThat(result.get(0).getSameAs(), is("odpt.StationFacility:TokyoMetro.Iidabashi"));


        checkBarrierfreeFacility(result);

        checkPlatformInfomation(result);

    }

    private void checkPlatformInfomation(final List<StationFacility> result) {
        List<PlatformInformation> platformInformationList = result.get(0).getPlatformInformationList();
        assertThat(platformInformationList.size(), is(32));
        PlatformInformation information = platformInformationList.get(1);
        assertThat(information.getCarComposition(), is(10));
        assertThat(information.getCarNumber(), is(2));
        assertThat(information.getRailDirection(), is(RailDirection.SHINKIBA));
        assertThat(information.getTransferInformationList().size(), is(4));
        assertThat(information.getTransferInformationList().get(0).getRailWay(), is(RailWay.TOZAI));
        assertThat(information.getTransferInformationList().get(0).getNecessaryTime(), is(5));
        assertThat(information.getTransferInformationList().get(1).getRailWay(), is(RailWay.OEDO));
        assertThat(information.getTransferInformationList().get(1).getNecessaryTime(), is(4));
        assertThat(information.getTransferInformationList().get(2).getRailWay(), is(RailWay.NAMBOKU));
        assertThat(information.getTransferInformationList().get(2).getNecessaryTime(), is(5));
        assertThat(information.getTransferInformationList().get(3).getRailWay(), is(RailWay.JR_EAST));
        assertThat(information.getTransferInformationList().get(3).getNecessaryTime(), is(7));

        assertThat(information.getBarrierfreeFacility().size(), is(3));
        assertThat(information.getBarrierfreeFacility().get(0), is("odpt.StationFacility:TokyoMetro.Yurakucho.Iidabashi.Inside.Elevator.1"));
        assertThat(information.getBarrierfreeFacility().get(1), is("odpt.StationFacility:TokyoMetro.Yurakucho.Iidabashi.Inside.Escalator.1"));
        assertThat(information.getBarrierfreeFacility().get(2), is("odpt.StationFacility:TokyoMetro.Yurakucho.Iidabashi.Inside.Toilet.1"));

        assertThat(information.getSurroundingArea().size(), is(2));
        assertThat(information.getSurroundingArea().get(0), is("警視庁遺失物センター"));
        assertThat(information.getSurroundingArea().get(1), is("JCHO東京新宿メディカルセンター"));
    }

    private void checkBarrierfreeFacility(final List<StationFacility> result) {
        List<BarrierfreeFacility> barrierfreeFacilityList = result.get(0).getBarrierfreeFacilityList();
        assertThat(barrierfreeFacilityList.size(), is(25));
        BarrierfreeFacility barrierfreeFacility1 = barrierfreeFacilityList.get(2);
        assertThat(barrierfreeFacility1.getId(), is("urn:ucode:_00001C000000000000010000030C4BF3"));
        assertThat(barrierfreeFacility1.getType(), is("ug:Escalator"));
        assertThat(barrierfreeFacility1.getSameAs(), is("odpt.StationFacility:TokyoMetro.Yurakucho.Iidabashi.Inside.Escalator.2"));
        assertThat(barrierfreeFacility1.getCategoryName(), is("エスカレーター"));
        assertThat(barrierfreeFacility1.getPlaceName(), is("ホーム（中央・和光市寄り）～飯田橋方面改札"));
        assertThat(barrierfreeFacility1.getLocatedAreaName(), is("改札内"));

        List<BarrierfreeFacility.ServiceDetail> detailList1 = barrierfreeFacility1.getServiceDetailList();
        assertThat(detailList1.size(), is(4));
        assertThat(detailList1.get(0).getOperationDay(), is("土休日"));
        assertThat(detailList1.get(0).getDirection(), is("上り・下り"));
        assertThat(detailList1.get(0).getServieceStartTime(), is(""));
        assertThat(detailList1.get(0).getServiceEndTime(), is(""));

        BarrierfreeFacility barrierfreeFacility2 = barrierfreeFacilityList.get(11);
        assertThat(barrierfreeFacility2.getId(), is("urn:ucode:_00001C000000000000010000030C4BFC"));
        assertThat(barrierfreeFacility2.getType(), is("ug:Toilet"));
        assertThat(barrierfreeFacility2.getSameAs(), is("odpt.StationFacility:TokyoMetro.Yurakucho.Iidabashi.Inside.Toilet.2"));
        assertThat(barrierfreeFacility2.getCategoryName(), is("トイレ"));
        assertThat(barrierfreeFacility2.getPlaceName(), is("神楽坂下改札内広場"));
        assertThat(barrierfreeFacility2.getLocatedAreaName(), is("改札内"));
        assertThat(barrierfreeFacility2.getHasAssistant().size(), is(4));
        assertThat(barrierfreeFacility2.getHasAssistant().get(0), is("spac:WheelchairAssesible"));
        assertThat(barrierfreeFacility2.getHasAssistant().get(1), is("ug:BabyChair"));
        assertThat(barrierfreeFacility2.getHasAssistant().get(2), is("ug:BabyChangingTable"));
        assertThat(barrierfreeFacility2.getHasAssistant().get(3), is("ug:ToiletForOstomate"));
    }

}