package tokyometro4j;

import org.junit.Before;
import org.junit.Test;
import tokyometro4j.common.Operator;
import tokyometro4j.common.Station;
import tokyometro4j.entity.RailwayFare;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class RailwayFareSearchTypeTest {

    RailwayFareSearchType sut;

    @Before
    public void setUp() {
        sut = new TokyoMetro().datapoints().railwayFare();
    }

    @Test
    public void testSameAs() {
        sut.sameAs("odpt.RailwayFare:TokyoMetro.Ginza.TokyoMetro.Asakusa.Ginza.Tawaramachi");

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("owl:sameAs"));
        assertThat(sut.params.get(0).getValue(), is("odpt.RailwayFare:TokyoMetro.Ginza.TokyoMetro.Asakusa.Ginza.Tawaramachi"));

    }

    @Test
    public void testOperator() {
        sut.operator(Operator.TOKYOMETRO);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:operator"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Operator:TokyoMetro"));
    }

    @Test
    public void testFromStation() {
        sut.fromStation(Station.CHIYODA_AKASAKA);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:fromStation"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Station:TokyoMetro.Chiyoda.Akasaka"));
    }

    @Test
    public void testToStation() {
        sut.toStation(Station.CHIYODA_AKASAKA);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:toStation"));
        assertThat(sut.params.get(0).getValue(), is("odpt.Station:TokyoMetro.Chiyoda.Akasaka"));
    }

    @Test
    public void testTicketFare() {
        sut.ticketFare(120);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:ticketFare"));
        assertThat(sut.params.get(0).getValue(), is("120"));
    }

    @Test
    public void testChildTicketFare() {
        sut.childTicketFare(80);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:childTicketFare"));
        assertThat(sut.params.get(0).getValue(), is("80"));
    }

    @Test
    public void testIcCardFare() {
        sut.icCardFare(200);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:icCardFare"));
        assertThat(sut.params.get(0).getValue(), is("200"));
    }

    @Test
    public void testChildIcCardFare() {
        sut.childIcCardFare(140);

        assertThat(sut.params.size(), is(1));
        assertThat(sut.params.get(0).getName(), is("odpt:childIcCardFare"));
        assertThat(sut.params.get(0).getValue(), is("140"));
    }

    @Test
    public void testExecute_南北線白金高輪から飯田橋までの運賃取得() throws Exception {
        List<RailwayFare> result = sut.fromStation(Station.NAMBOKU_SHIROKANE_TAKANAWA).toStation(Station.NAMBOKU_IIDABASHI).execute();

        assertThat(result, is(not(nullValue())));
        assertThat(result.size(), is(1));

        RailwayFare response = result.get(0);
        assertThat(response.getId(), is("urn:ucode:_00001C000000000000010000030C974B"));
        assertThat(response.getType(), is("odpt:RailwayFare"));
        assertThat(response.getSameAs(), is("odpt.RailwayFare:TokyoMetro.Namboku.ShirokaneTakanawa.TokyoMetro.Namboku.Iidabashi"));
        assertThat(response.getDate(), is("2014-08-29T16:03:36+09:00"));
        assertThat(response.getOperator(), is(Operator.TOKYOMETRO));
        assertThat(response.getFromStation(), is(Station.NAMBOKU_SHIROKANE_TAKANAWA));
        assertThat(response.getToStation(), is(Station.NAMBOKU_IIDABASHI));
        assertThat(response.getTicketFare(), is(200));
        assertThat(response.getChildTicketFare(), is(100));
        assertThat(response.getIcCardFare(), is(195));
        assertThat(response.getChildIcCardFare(), is(97));
        assertThat(response.getContext(), is("http://vocab.tokyometroapp.jp/context_odpt_RailwayFare.jsonld"));

    }
}