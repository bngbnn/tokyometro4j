package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailWay;
import tokyometro4j.entity.Station;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class DatapointsStationSearchType extends SearchType {

    protected final SearchApi searchApi;

    protected final String type = "odpt:Station";

    protected List<NameValuePair> params = new ArrayList<>();

    public DatapointsStationSearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    //    @id	URN	固有識別子(ucode)
    public DatapointsStationSearchType id(String id) {
        this.params.add(new BasicNameValuePair("@id", id));
        return this;
    }

    //    owl:sameAs	URL	固有識別子。命名ルールは、odpt.Train:TokyoMetro.路線名.列車番号である。
    public DatapointsStationSearchType sameAs(String train) {
        this.params.add(new BasicNameValuePair("owl:sameAs", train));
        return this;
    }

    //     dc:title	xsd:string	駅名
    public DatapointsStationSearchType title(String title) {
        this.params.add(new BasicNameValuePair("dc:title", title));
        return this;
    }

    //     odpt:operator	odpt:Operator	運行会社
    public DatapointsStationSearchType operator(Operator operator) {
        this.params.add(new BasicNameValuePair("odpt:operator", operator.getValue()));
        return this;
    }

    //     odpt:railway	odpt:Railway	路線
    public DatapointsStationSearchType railway(RailWay railWay) {
        this.params.add(new BasicNameValuePair("odpt:railway", railWay.getValue()));
        return this;
    }

    //     odpt:stationCode	xsd:string	駅コード
    public DatapointsStationSearchType stationCode(String stationCode) {
        this.params.add(new BasicNameValuePair("odpt:stationCode", stationCode));
        return this;
    }

    public List<Station> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<Station> station = new ArrayList<>();

        for (Map<String, Object> map : list) {
            station.add(Station.parse(map));
        }

        return station;
    }

}
