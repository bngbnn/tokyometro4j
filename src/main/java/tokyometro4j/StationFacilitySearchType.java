package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.entity.StationFacility;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 列車番号情報の検索を行うクラスです
 */
public class StationFacilitySearchType extends SearchType {

    /**
     * 検索API
     */
    protected final SearchApi searchApi;

    /**
     * 検索パラメーター
     */
    protected List<NameValuePair> params = new ArrayList<>();

    /**
     * 検索タイプ
     */
    private String type = "odpt:StationFacility";

    /**
     * コンストラクタ
     *
     * @param searchApi 検索API
     */
    public StationFacilitySearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }


    /**
     * 固有識別子の検索条件を設定します。
     *
     * @param station 固有識別子。命名ルールは、odpt.StationFacility:TokyoMetro.駅名 である。
     * @return インスタンス
     */
    public StationFacilitySearchType sameAs(String station) {
        this.params.add(new BasicNameValuePair("owl:sameAs", station));
        return this;
    }

    /**
     * 列車運行情報
     *
     * @return 検索結果のリスト
     * @throws java.net.URISyntaxException URI不正
     * @throws java.io.IOException         通信エラー
     */
    public List<StationFacility> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<StationFacility> stationFacilities = new ArrayList<>();

        for (Map<String, Object> map : list) {
            stationFacilities.add(StationFacility.parse(map));
        }

        return stationFacilities;
    }
}
