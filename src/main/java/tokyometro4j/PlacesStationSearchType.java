package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.entity.Station;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 */
public class PlacesStationSearchType extends SearchType {

    protected final SearchApi searchApi;

    protected final String type = "odpt:Station";

    private static final List<String> REQUIRED_PARAM_LIST = Arrays.asList("lat", "lon", "radius");

    protected List<NameValuePair> params = new ArrayList<>();

    public PlacesStationSearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    public PlacesStationSearchType lat(double latitude) {
        this.params.add(new BasicNameValuePair("lat", String.valueOf(latitude)));
        return this;
    }

    public PlacesStationSearchType lon(double longitude) {
        this.params.add(new BasicNameValuePair("lon", String.valueOf(longitude)));
        return this;
    }

    /**
     * 検索範囲の半径を指定します.
     *
     * @param radius 検索範囲の半径(m)
     * @return
     */
    public PlacesStationSearchType radius(double radius) {
        this.params.add(new BasicNameValuePair("radius", String.valueOf(radius)));
        return this;
    }

    public List<Station> execute() throws URISyntaxException, IOException {
        if (!hasRequiredParameters()) {
            throw new IOException("lat, lon, radius is required parameters.");
        }

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<Station> station = new ArrayList<>();

        for (Map<String, Object> map : list) {
            station.add(Station.parse(map));
        }

        return station;
    }

    private boolean hasRequiredParameters() {
        if (params.isEmpty()) {
            return false;
        }

        List<String> paramList = new ArrayList<>();
        for (NameValuePair nameValuePair : params) {
            paramList.add(nameValuePair.getName());
        }

        return paramList.containsAll(REQUIRED_PARAM_LIST);
    }
}
