package tokyometro4j.util;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class JsonUtil {

    public static String getJsonString(Map<String, Object> jsonMap, String key) {
        if (jsonMap == null) {
            return "";
        }

        if (jsonMap.get(key) == null) {
            return "";
        }

        return (String) jsonMap.get(key);
    }

    public static List getJsonList(Map<String, Object> jsonMap, String key) {
        if (jsonMap == null) {
            return Collections.emptyList();
        }

        if (jsonMap.get(key) == null) {
            return Collections.emptyList();
        }

        return (List) jsonMap.get(key);
    }

    public static int getJsonInt(Map<String, Object> jsonMap, String key) {
        if (jsonMap == null) {
            return 0;
        }

        if (jsonMap.get(key) == null) {
            return 0;
        }
        if (jsonMap.get(key) instanceof BigDecimal) {
            return ((BigDecimal) jsonMap.get(key)).intValue();
        } else {
            return (int) jsonMap.get(key);
        }
    }

    public static boolean getJsonBoolean(final Map<String, Object> jsonMap, final String key) {
        if (jsonMap == null) {
            return false;
        }

        if (jsonMap.get(key) == null) {
            return false;
        }

        return Boolean.valueOf((Boolean) jsonMap.get(key));
    }

    public static double getJsonDouble(final Map<String, Object> jsonMap, final String key) {
        if (jsonMap == null) {
            return 0.0;
        }

        if (jsonMap.get(key) == null) {
            return 0.0;
        }

        if (jsonMap.get(key) instanceof BigDecimal) {
            return ((BigDecimal) jsonMap.get(key)).doubleValue();
        } else {
            return (double) jsonMap.get(key);
        }

    }

    public static long getJsonLong(final Map jsonMap, final String key) {
        if (jsonMap == null) {
            return 0;
        }

        if (jsonMap.get(key) == null) {
            return 0;
        }

        if (jsonMap.get(key) instanceof BigDecimal) {
            return ((BigDecimal) jsonMap.get(key)).longValue();
        } else {
            return (long) jsonMap.get(key);
        }
    }
}
