package tokyometro4j.util;

import net.arnx.jsonic.JSON;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Http 関連のUtilです
 */
public class HttpUtil {

    public static List<Map<String, Object>> getApiResponse(final URI uri) throws IOException {
        System.out.println(uri);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(uri);
        HttpResponse response = httpClient.execute(httpGet);
        if (HttpStatus.SC_OK != response.getStatusLine().getStatusCode()) {
            throw new IOException("request failed : " + uri + " : " + response.toString());
        }
        List result = JSON.decode(response.getEntity().getContent());
        System.out.println(result.toString());

        return result;
    }
}
