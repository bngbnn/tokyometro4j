package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.StationTimetableObject;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailDirection;
import tokyometro4j.common.RailWay;
import tokyometro4j.common.Station;
import tokyometro4j.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class StationTimetable extends SearchResponse {

    protected String sameAs;
    protected String date;
    protected Station station;
    protected RailWay railway;
    protected Operator operator;
    protected RailDirection railDirection;
    protected List<StationTimetableObject> weekdays;
    protected List<StationTimetableObject> saturdays;
    protected List<StationTimetableObject> holidays;

    public static StationTimetable parse(final Map<String, Object> m) {
        StationTimetable stationTimetable = new StationTimetable();

        stationTimetable.context = JsonUtil.getJsonString(m, "@context");
        stationTimetable.id = JsonUtil.getJsonString(m, "@id");
        stationTimetable.type = JsonUtil.getJsonString(m, "@type");
        stationTimetable.sameAs = JsonUtil.getJsonString(m, "owl:sameAs");
        stationTimetable.date = JsonUtil.getJsonString(m, "dc:date");
        stationTimetable.station = Station.getStation(JsonUtil.getJsonString(m, "odpt:station"));
        stationTimetable.railway = RailWay.getRailWay(JsonUtil.getJsonString(m, "odpt:railway"));
        stationTimetable.operator = Operator.getOperator(JsonUtil.getJsonString(m, "odpt:operator"));
        stationTimetable.railDirection = RailDirection.getRailDirection(JsonUtil.getJsonString(m, "odpt:railDirection"));
        stationTimetable.weekdays = new ArrayList<>();
        List<Map> wlist = JsonUtil.getJsonList(m, "odpt:weekdays");
        for (Map wm : wlist) {
            stationTimetable.weekdays.add(StationTimetableObject.parse(wm));
        }
        stationTimetable.saturdays = new ArrayList<>();
        List<Map> slist = JsonUtil.getJsonList(m, "odpt:saturdays");
        for (Map sm : slist) {
            stationTimetable.saturdays.add(StationTimetableObject.parse(sm));
        }
        stationTimetable.holidays = new ArrayList<>();
        List<Map> hlist = JsonUtil.getJsonList(m, "odpt:holidays");
        for (Map hm : hlist) {
            stationTimetable.saturdays.add(StationTimetableObject.parse(hm));
        }
        return stationTimetable;
    }

    public String getSameAs() {
        return sameAs;
    }

    public String getDate() {
        return date;
    }

    public Station getStation() {
        return station;
    }

    public RailWay getRailway() {
        return railway;
    }

    public Operator getOperator() {
        return operator;
    }

    public RailDirection getRailDirection() {
        return railDirection;
    }

    public List<StationTimetableObject> getWeekdays() {
        return weekdays;
    }

    public List<StationTimetableObject> getSaturdays() {
        return saturdays;
    }

    public List<StationTimetableObject> getHolidays() {
        return holidays;
    }
}
