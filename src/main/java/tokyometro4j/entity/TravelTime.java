package tokyometro4j.entity;

import tokyometro4j.common.Station;
import tokyometro4j.common.TrainType;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 駅間の標準所要時間を表すエンティティクラス.
 * <p>
 * <table>
 * <tr>
 * <td>odpt:fromStation</td>
 * <td>odpt:Station</td>
 * <td>駅間の起点</td>
 * </tr>
 * <tr>
 * <td>odpt:toStation</td>
 * <td>odpt:Station</td>
 * <td>駅間の終点</td>
 * </tr>
 * <tr>
 * <td>odpt:necessaryTime</td>
 * <td>odpt:integer</td>
 * <td>駅間の所要時間（分）</td>
 * </tr>
 * <tr>
 * <td>odpt:trainType</td>
 * <td>odpt:TrainType</td>
 * <td>列車種別</td>
 * </tr>
 * </table>
 *
 * @see tokyometro4j.common.Station
 * @see tokyometro4j.common.TrainType
 */
public class TravelTime {

    protected Station fromStation;
    protected Station toStation;
    protected int necessaryTime;
    protected TrainType trainType;

    public static TravelTime parse(final Map<String, Object> m) {
        TravelTime travelTime = new TravelTime();

        travelTime.fromStation = Station.getStation(JsonUtil.getJsonString(m, "odpt:fromStation"));
        travelTime.toStation = Station.getStation(JsonUtil.getJsonString(m, "odpt:toStation"));
        travelTime.necessaryTime = JsonUtil.getJsonInt(m, "odpt:necessaryTime");
        travelTime.trainType = TrainType.getTrainType(JsonUtil.getJsonString(m, "odpt:trainType"));

        return travelTime;
    }

    public Station getFromStation() {
        return fromStation;
    }

    public Station getToStation() {
        return toStation;
    }

    public int getNecessaryTime() {
        return necessaryTime;
    }

    public TrainType getTrainType() {
        return trainType;
    }
}