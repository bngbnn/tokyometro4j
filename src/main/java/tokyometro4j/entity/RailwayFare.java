package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.common.Operator;
import tokyometro4j.common.Station;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 駅施設情報を格納するエンティティです
 */
public class RailwayFare extends SearchResponse {

    /**
     * owl:sameAs	URL	固有識別子。命名ルールはodpt.RailwayFare:TokyoMetro.出発駅の路線名.出発駅名.TokyoMetro.到着駅の路線名.到着駅名 である。
     */
    protected String sameAs;

    /**
     * dc:date	xsd:dateTime	データ生成日時（ISO8601 日付時刻形式）
     */
    protected String date;

    /**
     * odpt:operator	odpt:Operator	運行会社
     */
    protected Operator operator;

    /**
     * odpt:fromStation	odpt:Station	駅間の始点駅
     */
    protected Station fromStation;

    /**
     * odpt:toStation	odpt:Station	駅間の終点駅
     */
    protected Station toStation;

    /**
     * odpt:ticketFare	xsd:integer	切符利用時の運賃
     */
    protected int ticketFare;

    /**
     * odpt:childTicketFare	xsd:string	切符利用時の子供運賃
     */
    protected int childTicketFare;

    /**
     * odpt:icCardFare	xsd:integer	ICカード利用時の運賃
     */
    protected int icCardFare;

    /**
     * odpt:childIcCardFare	xsd:integer	ICカード利用時の子供運賃
     */
    protected int childIcCardFare;

    /**
     * Jsonからエンティティにつめ替えます
     *
     * @param json Jsonの解析結果
     * @return エンティティ
     */
    public static RailwayFare parse(final Map<String, Object> json) {
        RailwayFare railwayFare = new RailwayFare();

        railwayFare.context = JsonUtil.getJsonString(json, "@context");
        railwayFare.id = JsonUtil.getJsonString(json, "@id");
        railwayFare.type = JsonUtil.getJsonString(json, "@type");
        railwayFare.sameAs = JsonUtil.getJsonString(json, "owl:sameAs");
        railwayFare.date = JsonUtil.getJsonString(json, "dc:date");
        railwayFare.operator = Operator.getOperator(JsonUtil.getJsonString(json, "odpt:operator"));
        railwayFare.fromStation = Station.getStation(JsonUtil.getJsonString(json, "odpt:fromStation"));
        railwayFare.toStation = Station.getStation(JsonUtil.getJsonString(json, "odpt:toStation"));
        railwayFare.ticketFare = JsonUtil.getJsonInt(json, "odpt:ticketFare");
        railwayFare.childTicketFare = JsonUtil.getJsonInt(json, "odpt:childTicketFare");
        railwayFare.icCardFare = JsonUtil.getJsonInt(json, "odpt:icCardFare");
        railwayFare.childIcCardFare = JsonUtil.getJsonInt(json, "odpt:childIcCardFare");

        return railwayFare;
    }

    public String getSameAs() {
        return sameAs;
    }

    public String getDate() {
        return date;
    }

    public Operator getOperator() {
        return operator;
    }

    public Station getFromStation() {
        return fromStation;
    }

    public Station getToStation() {
        return toStation;
    }

    public int getTicketFare() {
        return ticketFare;
    }

    public int getChildTicketFare() {
        return childTicketFare;
    }

    public int getIcCardFare() {
        return icCardFare;
    }

    public int getChildIcCardFare() {
        return childIcCardFare;
    }
}