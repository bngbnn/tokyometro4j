package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.StationOrder;
import tokyometro4j.common.Operator;
import tokyometro4j.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 鉄道路線の情報を表すエンティティクラス.
 * <p>
 * <table>
 * <tr>
 * <td>owl:sameAs</td>
 * <td>URL</td>
 * <td>固有識別子。命名ルールは、odpt.Railway:TokyoMetro.路線名 である。</td>
 * </tr>
 * <tr>
 * <td>dc:date</td>
 * <td>xsd:dateTime</td>
 * <td>駅情報の生成時刻(ISO8601 日付時刻形式)</td>
 * </tr>
 * <tr>
 * <td>dc:title</td>
 * <td>xsd:string</td>
 * <td>運行系統名</td>
 * </tr>
 * <tr>
 * <td>ug:region</td>
 * <td>odpt:GeoDocument</td>
 * <td>路線形状データをGeojsonで取得するためのURL。取得時にはアクセストークンの付与が必要。</td>
 * </tr>
 * <tr>
 * <td>odpt:operator</td>
 * <td>odpt:Operator</td>
 * <td>運行会社</td>
 * </tr>
 * <tr>
 * <td>odpt:stationOrder</td>
 * <td>Array</td>
 * <td>駅の順序</td>
 * </tr>
 * <tr>
 * <td>odpt:travelTime</td>
 * <td>Array</td>
 * <td>駅間の標準所要時間リスト</td>
 * </tr>
 * <tr>
 * <td>odpt:lineCode</td>
 * <td>xsd:string</td>
 * <td>路線コードを格納</td>
 * </tr>
 * <tr>
 * <td>odpt:womenOnlyCar</td>
 * <td>Array</td>
 * <td>女性専用車両情報のリスト</td>
 * </tr>
 * </table>
 */
public class Railway extends SearchResponse {

    protected String sameAs;
    protected String date;
    protected String title;
    protected String region;
    protected Operator operator;
    protected List<StationOrder> stationOrders;
    protected List<TravelTime> travelTimes;
    protected String lineCode;
    protected List<WomenOnlyCar> womenOnlyCars;

    public static Railway parse(final Map<String, Object> m) {
        Railway railway = new Railway();

        System.out.println(m);
        railway.context = JsonUtil.getJsonString(m, "@context");
        railway.id = JsonUtil.getJsonString(m, "@id");
        railway.type = JsonUtil.getJsonString(m, "@type");
        railway.sameAs = JsonUtil.getJsonString(m, "owl:sameAs");
        railway.date = JsonUtil.getJsonString(m, "dc:date");
        railway.title = JsonUtil.getJsonString(m, "dc:title");
        railway.region = JsonUtil.getJsonString(m, "ug:region");
        railway.operator = Operator.getOperator(JsonUtil.getJsonString(m, "odpt:operator"));
        railway.stationOrders = new ArrayList<>();
        List<Map> solist = JsonUtil.getJsonList(m, "odpt:stationOrder");
        for (Map sm : solist) {
            railway.stationOrders.add(StationOrder.parse(sm));
        }
        railway.travelTimes = new ArrayList<>();
        List<Map> ttlist = JsonUtil.getJsonList(m, "odpt:travelTime");
        for (Map tm : ttlist) {
            railway.travelTimes.add(TravelTime.parse(tm));
        }
        railway.lineCode = JsonUtil.getJsonString(m, "odpt:lineCode");
        railway.womenOnlyCars = new ArrayList<>();
        List<Map> woclist = JsonUtil.getJsonList(m, "odpt:womenOnlyCar");
        for (Map wm : woclist) {
            railway.womenOnlyCars.add(WomenOnlyCar.parse(wm));
        }

        return railway;
    }

    public String getSameAs() {
        return sameAs;
    }

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getRegion() {
        return region;
    }

    public Operator getOperator() {
        return operator;
    }

    public List<StationOrder> getStationOrders() {
        return stationOrders;
    }

    public List<TravelTime> getTravelTimes() {
        return travelTimes;
    }

    public String getLineCode() {
        return lineCode;
    }

    public List<WomenOnlyCar> getWomenOnlyCars() {
        return womenOnlyCars;
    }
}
