package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.common.RailDirection;
import tokyometro4j.common.RailWay;
import tokyometro4j.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 駅施設を格納するエンティティです
 */
public class PlatformInformation extends SearchResponse {

    /**
     * odpt:carComposition		xsd:integer	車両編成数	◯
     */
    protected int carComposition;

    /**
     * odpt:carNumber		xsd:integer	車両の号車番号	◯
     */
    protected int carNumber;

    /**
     * odpt:railDirection		odpt:RailDirection	プラットフォームに停車する列車の方面	◯
     */
    protected RailDirection railDirection;

    /**
     * odpt:transferInformation		Array	最寄りの乗り換え可能な路線と所要時間
     */
    protected List<TransferInformation> transferInformationList;

    /**
     * odpt:barrierfreeFacility		Array(ug:SpatialThing)	最寄りのバリアフリー施設
     * sameAsのリストがかえってくる
     */
    protected List<String> barrierfreeFacility;

    /**
     * odpt:surroundingArea		Array(xsd:string)	改札外の最寄り施設
     */
    protected List<String> surroundingArea;

    /**
     * 乗り換えに関する情報を表すクラス
     */
    public static class TransferInformation {

        /**
         * odpt:railway	odpt:Railway	乗り換え可能路線
         */
        protected RailWay railWay;

        /**
         * odpt:railDirection	odpt:RailDirection	乗り換え可能路線の方面。乗り換え可能な方面を特記する必要がある場合にのみ記載。
         */
        protected RailDirection railDirection;

        /**
         * odpt:necessaryTime	xsd:integer	所要時間（分）
         */
        protected int necessaryTime;

        /**
         * Jsonからエンティティにつめ替えます
         *
         * @param json Jsonの解析結果
         * @return エンティティ
         */
        public static TransferInformation parse(final Map<String, Object> json) {
            TransferInformation transferInformation = new TransferInformation();
            transferInformation.railWay = RailWay.getRailWay(JsonUtil.getJsonString(json, "odpt:railway"));
            transferInformation.railDirection = RailDirection.getRailDirection(JsonUtil.getJsonString(json, "odpt:railDirection"));
            transferInformation.necessaryTime = JsonUtil.getJsonInt(json, "odpt:necessaryTime");
            return transferInformation;
        }

        public RailDirection getRailDirection() {
            return railDirection;
        }

        public RailWay getRailWay() {
            return railWay;
        }

        public int getNecessaryTime() {
            return necessaryTime;
        }
    }

    /**
     * Jsonからエンティティにつめ替えます
     *
     * @param json Jsonの解析結果
     * @return エンティティ
     */
    public static PlatformInformation parse(final Map<String, Object> json) {
        PlatformInformation platformInformation = new PlatformInformation();
        platformInformation.carComposition = JsonUtil.getJsonInt(json, "odpt:carComposition");
        platformInformation.carNumber = JsonUtil.getJsonInt(json, "odpt:carNumber");
        platformInformation.railDirection = RailDirection.getRailDirection(JsonUtil.getJsonString(json, "odpt:railDirection"));

        platformInformation.transferInformationList = new ArrayList<>();
        List<Map> tList = JsonUtil.getJsonList(json, "odpt:transferInformation");
        for (Map tMap : tList) {
            platformInformation.transferInformationList.add(TransferInformation.parse(tMap));
        }

        platformInformation.barrierfreeFacility = JsonUtil.getJsonList(json, "odpt:barrierfreeFacility");
        platformInformation.surroundingArea = JsonUtil.getJsonList(json, "odpt:surroundingArea");
        return platformInformation;
    }

    public int getCarComposition() {
        return carComposition;
    }

    public int getCarNumber() {
        return carNumber;
    }

    public RailDirection getRailDirection() {
        return railDirection;
    }

    public List<TransferInformation> getTransferInformationList() {
        return transferInformationList;
    }

    public List<String> getBarrierfreeFacility() {
        return barrierfreeFacility;
    }

    public List<String> getSurroundingArea() {
        return surroundingArea;
    }
}