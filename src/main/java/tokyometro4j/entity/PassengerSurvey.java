package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.common.Operator;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 駅乗降人員数を表すエンティティです
 *
 * @context URL    JSON-LD仕様に基づく @context のURL	◯
 * @id URN    固有識別子(ucode)	◯
 * @type odpt:PassengerSurvey	クラス名、odpt:PassengerSurvey	◯ owl:sameAs	URL	固有識別子。 命名ルールは odpt.PassengerSurvey:TokyoMetro.駅名.調査年 である。e.g. odpt:PassengerSurvey:TokyoMetro.Tokyo.2013	◯
 * odpt:operator	odpt:Operator	運行会社	◯
 * odpt:surveyYear	xsd:integer	調査年度	◯
 * odpt:passengerJourneys	xsd:integer	駅の1日あたりの平均乗降人員数	◯
 */
public class PassengerSurvey extends SearchResponse {

    /**
     * 固有識別子
     */
    protected String sameAs;

    /**
     * 運行会社
     */
    protected Operator operator;

    /**
     * 調査年度
     */
    protected int surveyYear;

    /**
     * 駅の1日あたりの平均乗降人員数
     */
    protected int passengerJourneys;


    public static PassengerSurvey parse(final Map<String, Object> json) {

        PassengerSurvey passengerSurvey = new PassengerSurvey();

        passengerSurvey.context = JsonUtil.getJsonString(json, "@context");
        passengerSurvey.id = JsonUtil.getJsonString(json, "@id");
        passengerSurvey.type = JsonUtil.getJsonString(json, "@type");
        passengerSurvey.sameAs = JsonUtil.getJsonString(json, "owl:sameAs");
        passengerSurvey.operator = Operator.getOperator(JsonUtil.getJsonString(json, "odpt:operator"));
        passengerSurvey.surveyYear = JsonUtil.getJsonInt(json, "odpt:surveyYear");
        passengerSurvey.passengerJourneys = JsonUtil.getJsonInt(json, "odpt:passengerJourneys");
        return passengerSurvey;
    }

    /**
     * get sameAs
     *
     * @return sameAs
     */
    public String getSameAs() {
        return sameAs;
    }

    /**
     * get Operator
     *
     * @return operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * get SurveyYear
     *
     * @return SurveyYear
     */
    public int getSurveyYear() {
        return surveyYear;
    }

    /**
     * get PassengerJourneys
     *
     * @return PassengerJourneys
     */
    public int getPassengerJourneys() {
        return passengerJourneys;
    }
}
