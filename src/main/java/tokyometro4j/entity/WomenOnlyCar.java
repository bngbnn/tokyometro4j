package tokyometro4j.entity;

import tokyometro4j.common.Station;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 女性専用車両情報を表すエンティティクラス.
 * <p>
 * <table>
 * <tr>
 * <td>odpt:fromStation</td>
 * <td>odpt:Station</td>
 * <td>女性専用車両開始駅</td>
 * </tr>
 * <tr>
 * <td>odpt:toStation</td>
 * <td>odpt:Station</td>
 * <td>女性専用車両開始駅</td>
 * </tr>
 * <tr>
 * <td>odpt:operationDay</td>
 * <td>xsd:string</td>
 * <td>女性専用車両実施曜日</td>
 * </tr>
 * <tr>
 * <td>odpt:availableTimeFrom</td>
 * <td>odpt:Time</td>
 * <td>女性専用車両開始時間</td>
 * </tr>
 * <tr>
 * <td>odpt:availableTimeUntil</td>
 * <td>odpt:Time</td>
 * <td>女性専用車両終了時間</td>
 * </tr>
 * <tr>
 * <td>odpt:carComposition</td>
 * <td>xsd:integer</td>
 * <td>車両編成数</td>
 * </tr>
 * <tr>
 * <td>odpt:carNumber</td>
 * <td>Array(xsd:integer)</td>
 * <td>女性専用車両実施車両号車番号</td>
 * </tr>
 * </table>
 *
 * @see tokyometro4j.common.Station
 */
public class WomenOnlyCar {

    protected Station fromStation;
    protected Station toStation;
    protected String operationDay;
    protected String availableTimeFrom;
    protected String availableTimeUntil;
    protected int carComposition;
    protected int carNumbers;

    public static WomenOnlyCar parse(final Map<String, Object> m) {
        WomenOnlyCar womenOnlyCar = new WomenOnlyCar();

        womenOnlyCar.fromStation = Station.getStation(JsonUtil.getJsonString(m, "odpt:fromStation"));
        womenOnlyCar.toStation = Station.getStation(JsonUtil.getJsonString(m, "odpt:toStation"));
        womenOnlyCar.operationDay = JsonUtil.getJsonString(m, "odpt:operationDay");
        womenOnlyCar.availableTimeFrom = JsonUtil.getJsonString(m, "odpt:availableTimeFrom");
        womenOnlyCar.availableTimeUntil = JsonUtil.getJsonString(m, "odpt:availableTimeUntil");
        womenOnlyCar.carComposition = JsonUtil.getJsonInt(m, "odpt:carComposition");
        womenOnlyCar.carNumbers = JsonUtil.getJsonInt(m, "odpt:carNumber");

        return womenOnlyCar;
    }

    public Station getFromStation() {
        return fromStation;
    }

    public Station getToStation() {
        return toStation;
    }

    public String getOperationDay() {
        return operationDay;
    }

    public String getAvailableTimeFrom() {
        return availableTimeFrom;
    }

    public String getAvailableTimeUntil() {
        return availableTimeUntil;
    }

    public int getCarComposition() {
        return carComposition;
    }

    public int getCarNumbers() {
        return carNumbers;
    }
}
