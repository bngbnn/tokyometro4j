package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.common.RailDirection;
import tokyometro4j.common.RailWay;
import tokyometro4j.common.Station;
import tokyometro4j.common.TrainOwner;
import tokyometro4j.common.TrainType;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 *
 */
public class Train extends SearchResponse {

    protected String sameAs;
    protected String trainNumber;
    protected TrainType trainType;
    protected String date;
    protected String valid;
    protected long frequency;
    protected RailWay railway;
    protected TrainOwner trainOwner;
    protected RailDirection railDirection;
    protected int delay;
    protected Station startingStation;
    protected Station terminalStation;
    protected Station fromStation;
    protected Station toStation;

    public static Train parse(final Map<String, Object> json) {
        Train train = new Train();

        train.context = JsonUtil.getJsonString(json, "@context");
        train.id = JsonUtil.getJsonString(json, "@id");
        train.type = JsonUtil.getJsonString(json, "@type");
        train.sameAs = JsonUtil.getJsonString(json, "owl:sameAs");
        train.trainNumber = JsonUtil.getJsonString(json, "odpt:trainNumber");
        train.trainType = TrainType.getTrainType(JsonUtil.getJsonString(json, "odpt:trainType"));
        train.date = JsonUtil.getJsonString(json, "dc:date");
        train.valid = JsonUtil.getJsonString(json, "dct:valid");
        train.frequency = JsonUtil.getJsonLong(json, "odpt:frequency");
        train.railway = RailWay.getRailWay(JsonUtil.getJsonString(json, "odpt:railway"));
        train.trainOwner = TrainOwner.getTrainOwner(JsonUtil.getJsonString(json, "odpt:trainOwner"));
        train.railDirection = RailDirection.getRailDirection(JsonUtil.getJsonString(json, "odpt:railDirection"));
        train.delay = JsonUtil.getJsonInt(json, "odpt:delay");
        train.startingStation = Station.getStation(JsonUtil.getJsonString(json, "odpt:startingStation"));
        train.terminalStation = Station.getStation(JsonUtil.getJsonString(json, "odpt:terminalStation"));
        train.fromStation = Station.getStation(JsonUtil.getJsonString(json, "odpt:fromStation"));
        train.toStation = Station.getStation(JsonUtil.getJsonString(json, "odpt:toStation"));

        return train;
    }

    public String getSameAs() {
        return sameAs;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public String getDate() {
        return date;
    }

    public String getValid() {
        return valid;
    }

    public long getFrequency() {
        return frequency;
    }

    public RailWay getRailway() {
        return railway;
    }

    public TrainOwner getTrainOwner() {
        return trainOwner;
    }

    public RailDirection getRailDirection() {
        return railDirection;
    }

    public int getDelay() {
        return delay;
    }

    public Station getStartingStation() {
        return startingStation;
    }

    public Station getTerminalStation() {
        return terminalStation;
    }

    public Station getFromStation() {
        return fromStation;
    }

    public Station getToStation() {
        return toStation;
    }
}

