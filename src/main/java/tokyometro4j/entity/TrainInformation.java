package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailWay;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 列車運行情報を格納するエンティティです
 */
public class TrainInformation extends SearchResponse {

    /**
     * dc:date	xsd:dateTime	データ生成時刻、e.g. 2013–01–13T15:10:00+09:00、ISO8601 日付時刻形式	◯
     */
    protected String date;

    /**
     * dct:valid	xsd:dateTime	有効期限（ISO8601 日付時刻形式）	◯
     */
    protected String valid;

    /**
     * odpt:operator	odpt:Operator	運行会社	◯
     */
    protected Operator operator;

    /**
     * odpt:timeOfOrigin	xsd:dateTime	発生時刻（ISO8601 日付時刻形式）	◯
     */
    protected String timeOfOrigin;

    /**
     * odpt:railway	odpt:Railway	発生路線	◯
     */
    protected RailWay railway;

    /**
     * odpt:trainInformationStatus	xsd:string	平常時は省略。運行情報が存在する場合は「運行情報あり」を格納。遅延などの情報を取得可能な場合は、「遅延」等のテキストを格納。
     */
    protected String trainInformationStatus;

    /**
     * odpt:trainInformationText	xsd:string	運行情報テキスト	◯
     */
    protected String trainInformationText;

    /**
     * Jsonからエンティティにつめ替えます
     *
     * @param json Jsonの解析結果
     * @return エンティティ
     */
    public static TrainInformation parse(final Map<String, Object> json) {
        TrainInformation trainInformation = new TrainInformation();

        trainInformation.context = JsonUtil.getJsonString(json, "@context");
        trainInformation.id = JsonUtil.getJsonString(json, "@id");
        trainInformation.type = JsonUtil.getJsonString(json, "@type");
        trainInformation.date = JsonUtil.getJsonString(json, "dc:date");
        trainInformation.valid = JsonUtil.getJsonString(json, "dct:valid");
        trainInformation.operator = Operator.getOperator(JsonUtil.getJsonString(json, "odpt:operator"));
        trainInformation.timeOfOrigin = JsonUtil.getJsonString(json, "odpt:timeOfOrigin");
        trainInformation.railway = RailWay.getRailWay(JsonUtil.getJsonString(json, "odpt:railway"));
        trainInformation.trainInformationStatus = JsonUtil.getJsonString(json, "odpt:trainInformationStatus");
        trainInformation.trainInformationText = JsonUtil.getJsonString(json, "odpt:trainInformationText");

        return trainInformation;
    }

    /**
     * get date
     *
     * @return date
     */
    public String getDate() {
        return date;
    }

    /**
     * det valie
     *
     * @return value
     */
    public String getValid() {
        return valid;
    }

    /**
     * get operator
     *
     * @return operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * get time of origin
     *
     * @return timeOfOrigin
     */
    public String getTimeOfOrigin() {
        return timeOfOrigin;
    }

    /**
     * get railway
     *
     * @return Railway
     */
    public RailWay getRailway() {
        return railway;
    }

    /**
     * get TrainInformationStatus
     *
     * @return TrainInformationStatus
     */
    public String getTrainInformationStatus() {
        return trainInformationStatus;
    }

    /**
     * get TrainInformationText
     *
     * @return TrainInformationText
     */
    public String getTrainInformationText() {
        return trainInformationText;
    }
}

