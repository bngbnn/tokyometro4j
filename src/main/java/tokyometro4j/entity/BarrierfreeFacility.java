package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 駅施設を格納するエンティティです
 */
public class BarrierfreeFacility extends SearchResponse {

    /**
     * owl:sameAs		URL	固有識別子. 命名ルールはodpt.Facility:TokyoMetro.路線名.駅名.改札の内外.カテゴリ名.通し番号 である。
     */
    protected String sameAs;

    /**
     * ugsrv:categoryName		xsd:string	施設のカテゴリ名（トイレ、階段昇降機、エレベータ、エスカレータ、ハンドル型電動車いす利用可能経路）
     */
    protected String categoryName;

    /**
     * odpt:serviceDetail		Array	施設情報の詳細
     */
    protected List<ServiceDetail> serviceDetailList;

    /**
     * odpt:placeName		xsd:string	施設の設置されている場所の名前
     */
    protected String placeName;

    /**
     * odpt:locatedAreaName		xsd:string	施設の設置場所（改札内／改札外）
     */
    protected String locatedAreaName;

    /**
     * ugsrv:remark		xsd:string	補足事項
     */
    protected String remark;

    /**
     * spac:hasAssistant		spac:Assistant	トイレ内のバリアフリー施設を記述。 "spac:WheelchairAssessible", "ug:BabyChangingTable", "spac:Ostomate" が格納される。
     */
    protected List<String> hasAssistant;

    /**
     * spac:isAvailableTo		spac:MoverType	ハンドル型電動車いす利用可能経路の場合は"spac:MobilityScooter" が格納される。車いす対応エスカレータの場合は"spac:Wheelchair"が格納される。
     */
    protected String isAvailableTo;

    /**
     * 駅施設情報の詳細を表すクラス
     */
    public static class ServiceDetail {

        /**
         * ugsrv:serviceStartTime	xsd:string	施設の利用可能開始時間（いつでも利用できる場合は省略）。基本的にはISO8601時刻形式（05:30など）であるが、「始発」と入る場合もある。
         */
        protected String servieceStartTime;

        /**
         * ugsrv:serviceEndTime	xsd:string	施設の利用可能終了時間（いつでも利用できる場合は省略）。基本的にはISO8601時刻形式（23:50など）であるが、「終車時」と入る場合もある。
         */
        protected String serviceEndTime;

        /**
         * odpt:operationDay	xsd:string	施設利用可能時間やエスカレータの方向が曜日によって変わる場合に、次のいずれかを格納（曜日に依存しない場合は省略） 平日, 土休日, 土日祝, 日曜, 月曜, 火曜, 水曜, 木曜, 金曜, 土曜
         */
        protected String operationDay;

        /**
         * ug:direction	xsd:string	エスカレータの方向名（施設がエスカレータの場合に格納。上り、下り、上り・下りの3種類が存在）
         */
        protected String direction;

        /**
         * Jsonからエンティティにつめ替えます
         *
         * @param json Jsonの解析結果
         * @return エンティティ
         */
        public static ServiceDetail parse(final Map<String, Object> json) {
            ServiceDetail serviceDetail = new ServiceDetail();
            serviceDetail.servieceStartTime = JsonUtil.getJsonString(json, "ugsrv:serviceStartTime");
            serviceDetail.serviceEndTime = JsonUtil.getJsonString(json, "ugsrv:serviceEndTime");
            serviceDetail.operationDay = JsonUtil.getJsonString(json, "odpt:operationDay");
            serviceDetail.direction = JsonUtil.getJsonString(json, "ug:direction");
            return serviceDetail;
        }

        public String getServieceStartTime() {
            return servieceStartTime;
        }

        public String getServiceEndTime() {
            return serviceEndTime;
        }

        public String getOperationDay() {
            return operationDay;
        }

        public String getDirection() {
            return direction;
        }
    }

    /**
     * Jsonからエンティティにつめ替えます
     *
     * @param json Jsonの解析結果
     * @return エンティティ
     */
    public static BarrierfreeFacility parse(final Map<String, Object> json) {
        BarrierfreeFacility barrierfreeFacility = new BarrierfreeFacility();

        barrierfreeFacility.id = JsonUtil.getJsonString(json, "@id");
        barrierfreeFacility.type = JsonUtil.getJsonString(json, "@type");
        barrierfreeFacility.sameAs = JsonUtil.getJsonString(json, "owl:sameAs");
        barrierfreeFacility.categoryName = JsonUtil.getJsonString(json, "ugsrv:categoryName");

        barrierfreeFacility.serviceDetailList = new ArrayList<>();
        List<Map> sList = JsonUtil.getJsonList(json, "odpt:serviceDetail");
        for (Map sMap : sList) {
            barrierfreeFacility.serviceDetailList.add(ServiceDetail.parse(sMap));
        }

        barrierfreeFacility.placeName = JsonUtil.getJsonString(json, "odpt:placeName");
        barrierfreeFacility.locatedAreaName = JsonUtil.getJsonString(json, "odpt:locatedAreaName");
        barrierfreeFacility.remark = JsonUtil.getJsonString(json, "ugsrv:remark");
        barrierfreeFacility.hasAssistant = JsonUtil.getJsonList(json, "spac:hasAssistant");
        barrierfreeFacility.isAvailableTo = JsonUtil.getJsonString(json, "spac:isAvailableTo");

        return barrierfreeFacility;
    }

    public String getSameAs() {
        return sameAs;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<ServiceDetail> getServiceDetailList() {
        return serviceDetailList;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getLocatedAreaName() {
        return locatedAreaName;
    }

    public String getRemark() {
        return remark;
    }

    public List<String> getHasAssistant() {
        return hasAssistant;
    }

    public String getIsAvailableTo() {
        return isAvailableTo;
    }
}