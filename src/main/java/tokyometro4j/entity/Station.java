package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailWay;
import tokyometro4j.util.JsonUtil;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class Station extends SearchResponse {

    public String sameAs;
    public String title;
    public String date;
    public double longitude;
    public double latitude;
    public String region;
    public Operator operator;
    public RailWay railway;
    public List<String> connectingRailways;
    public String facility;
    public List<String> passengerSurveys;
    public String stationCode;
    public List<String> exits;

    public static Station parse(final Map<String, Object> m) {
        Station station = new Station();

        System.out.println(m);
        station.context = JsonUtil.getJsonString(m, "@context");
        station.id = JsonUtil.getJsonString(m, "@id");
        station.type = JsonUtil.getJsonString(m, "@type");
        station.sameAs = JsonUtil.getJsonString(m, "owl:sameAs");
        station.title = JsonUtil.getJsonString(m, "dc:title");
        station.date = JsonUtil.getJsonString(m, "dc:date");
        station.longitude = JsonUtil.getJsonDouble(m, "geo:long");
        station.latitude = JsonUtil.getJsonDouble(m, "geo:lat");
        station.region = JsonUtil.getJsonString(m, "ug:region");
        station.operator = Operator.getOperator(JsonUtil.getJsonString(m, "odpt:operator"));
        station.railway = RailWay.getRailWay(JsonUtil.getJsonString(m, "odpt:railway"));
        station.connectingRailways = JsonUtil.getJsonList(m, "odpt:connectingRailway");
        station.facility = JsonUtil.getJsonString(m, "odpt:facility");
        station.passengerSurveys = JsonUtil.getJsonList(m, "odpt:passengerSurvey");
        station.stationCode = JsonUtil.getJsonString(m, "odpt:stationCode");
        station.exits = JsonUtil.getJsonList(m, "odpt:exit");

        return station;
    }


    public String getSameAs() {
        return sameAs;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getRegion() {
        return region;
    }

    public Operator getOperator() {
        return operator;
    }

    public RailWay getRailway() {
        return railway;
    }

    public List<String> getConnectingRailways() {
        return connectingRailways;
    }

    public String getFacility() {
        return facility;
    }

    public List<String> getPassengerSurveys() {
        return passengerSurveys;
    }

    public String getStationCode() {
        return stationCode;
    }

    public List<String> getExits() {
        return exits;
    }
}