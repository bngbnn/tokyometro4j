package tokyometro4j.entity;

import tokyometro4j.SearchResponse;
import tokyometro4j.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 駅施設情報を格納するエンティティです
 */
public class StationFacility extends SearchResponse {

    /**
     * owl:sameAs URL	固有識別子、命名ルールは、odpt.StationFacility:TokyoMetro.駅名 である。	◯
     */
    protected String sameAs;

    /**
     * odpt:barrierfreeFacility	 Array(ug:SpatialThing)	駅の施設一覧
     */
    protected List<BarrierfreeFacility> barrierfreeFacilityList;

    /**
     * odpt:platformInformation			Array	プラットフォームに車両が停車している時の、車両毎の最寄りの施設・出口等の情報を記述	◯
     */
    protected List<PlatformInformation> platformInformationList;

    /**
     * Jsonからエンティティにつめ替えます
     *
     * @param json Jsonの解析結果
     * @return エンティティ
     */
    public static StationFacility parse(final Map<String, Object> json) {
        StationFacility stationFacility = new StationFacility();

        stationFacility.context = JsonUtil.getJsonString(json, "@context");
        stationFacility.id = JsonUtil.getJsonString(json, "@id");
        stationFacility.type = JsonUtil.getJsonString(json, "@type");
        stationFacility.sameAs = JsonUtil.getJsonString(json, "owl:sameAs");

        stationFacility.barrierfreeFacilityList = new ArrayList<>();
        List<Map> bList = JsonUtil.getJsonList(json, "odpt:barrierfreeFacility");
        for (Map bMap : bList) {
            stationFacility.barrierfreeFacilityList.add(BarrierfreeFacility.parse(bMap));
        }

        stationFacility.platformInformationList = new ArrayList<>();
        List<Map> pList = JsonUtil.getJsonList(json, "odpt:platformInformation");
        for (Map pMap : pList) {
            stationFacility.platformInformationList.add(PlatformInformation.parse(pMap));
        }

        return stationFacility;
    }

    public String getSameAs() {
        return sameAs;
    }

    public List<BarrierfreeFacility> getBarrierfreeFacilityList() {
        return barrierfreeFacilityList;
    }

    public List<PlatformInformation> getPlatformInformationList() {
        return platformInformationList;
    }
}