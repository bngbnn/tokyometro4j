package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.Operator;
import tokyometro4j.common.Station;
import tokyometro4j.entity.RailwayFare;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 列車番号情報の検索を行うクラスです
 */
public class RailwayFareSearchType extends SearchType {

    /**
     * 検索API
     */
    protected final SearchApi searchApi;

    /**
     * 検索パラメーター
     */
    protected List<NameValuePair> params = new ArrayList<>();

    /**
     * 検索タイプ
     */
    private String type = "odpt:RailwayFare";

    /**
     * コンストラクタ
     *
     * @param searchApi 検索API
     */
    public RailwayFareSearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }


    /**
     * 固有識別子の検索条件を設定します。
     *
     * @param station 固有識別子。命名ルールは、odpt.StationFacility:TokyoMetro.駅名 である。
     * @return インスタンス
     */
    public RailwayFareSearchType sameAs(String station) {
        this.params.add(new BasicNameValuePair("owl:sameAs", station));
        return this;
    }

    /**
     * 運行会社の検索条件を設定します。
     *
     * @param operator 運行会社
     * @return インスタンス
     */
    public RailwayFareSearchType operator(Operator operator) {
        this.params.add(new BasicNameValuePair("odpt:operator", operator.getValue()));
        return this;
    }

    /**
     * 駅間の始点駅の検索条件を設定します。
     *
     * @param station 駅間の始点駅
     * @return インスタンス
     */
    public RailwayFareSearchType fromStation(Station station) {
        this.params.add(new BasicNameValuePair("odpt:fromStation", station.getValue()));
        return this;
    }

    /**
     * 駅間の終点駅の検索条件を設定します。
     *
     * @param station 駅間の終点駅
     * @return インスタンス
     */
    public RailwayFareSearchType toStation(Station station) {
        this.params.add(new BasicNameValuePair("odpt:toStation", station.getValue()));
        return this;
    }

    /**
     * 切符利用時の運賃の検索条件を設定します。
     *
     * @param ticketFare 切符利用時の運賃
     * @return インスタンス
     */
    public RailwayFareSearchType ticketFare(int ticketFare) {
        this.params.add(new BasicNameValuePair("odpt:ticketFare", String.valueOf(ticketFare)));
        return this;
    }

    /**
     * 切符利用時の子供運賃の検索条件を設定します。
     *
     * @param childTicketFare 切符利用時の子供運賃
     * @return インスタンス
     */
    public RailwayFareSearchType childTicketFare(int childTicketFare) {
        this.params.add(new BasicNameValuePair("odpt:childTicketFare", String.valueOf(childTicketFare)));
        return this;
    }

    /**
     * ICカード利用時の運賃の検索条件を設定します。
     *
     * @param icCardFare ICカード利用時の運賃
     * @return インスタンス
     */
    public RailwayFareSearchType icCardFare(int icCardFare) {
        this.params.add(new BasicNameValuePair("odpt:icCardFare", String.valueOf(icCardFare)));
        return this;
    }

    /**
     * ICカード利用時の子供運賃の検索条件を設定します。
     *
     * @param childIcCardFare ICカード利用時の子供運賃
     * @return インスタンス
     */
    public RailwayFareSearchType childIcCardFare(int childIcCardFare) {
        this.params.add(new BasicNameValuePair("odpt:childIcCardFare", String.valueOf(childIcCardFare)));
        return this;
    }

    /**
     * 列車運行情報
     *
     * @return 検索結果のリスト
     * @throws java.net.URISyntaxException URI不正
     * @throws java.io.IOException         通信エラー
     */
    public List<RailwayFare> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<RailwayFare> railwayFareList = new ArrayList<>();

        for (Map<String, Object> map : list) {
            railwayFareList.add(RailwayFare.parse(map));
        }

        return railwayFareList;
    }
}
