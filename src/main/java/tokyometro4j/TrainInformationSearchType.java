package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.Operator;
import tokyometro4j.common.RailWay;
import tokyometro4j.entity.TrainInformation;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 列車番号情報の検索を行うクラスです
 */
public class TrainInformationSearchType extends SearchType {

    /**
     * 検索API
     */
    protected final SearchApi searchApi;

    /**
     * 検索パラメーター
     */
    protected List<NameValuePair> params = new ArrayList<>();

    /**
     * 検索タイプ
     */
    private String type = "odpt:TrainInformation";

    /**
     * コンストラクタ
     *
     * @param searchApi 検索API
     */
    public TrainInformationSearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    /**
     * 運行会社の検索条件を設定します
     *
     * @param operator 運行会社
     * @return インスタンス
     */
    public TrainInformationSearchType operator(Operator operator) {
        this.params.add(new BasicNameValuePair("odpt:operator", operator.getValue()));
        return this;
    }

    /**
     * 発生路線の検索条件を設定します
     *
     * @param railWay 路線
     * @return インスタンス
     */
    public TrainInformationSearchType railway(RailWay railWay) {
        this.params.add(new BasicNameValuePair("odpt:railway", railWay.getValue()));
        return this;
    }

    /**
     * ステータスの検索条件を設定します
     * <p>
     * 平常時は省略。運行情報が存在する場合は「運行情報あり」を格納。遅延などの情報を取得可能な場合は、「遅延」等のテキストを格納。
     *
     * @param status ステータス
     * @return インスタンス
     */
    public TrainInformationSearchType trainInformationStatus(String status) {
        this.params.add(new BasicNameValuePair("odpt:trainInformationStatus", status));
        return this;
    }

    /**
     * 運行情報テキストの検索条件を設定します
     *
     * @param text 運行情報テキスト
     * @return インスタンス
     */
    public TrainInformationSearchType trainInformationText(String text) {
        this.params.add(new BasicNameValuePair("odpt:trainInformationText", text));
        return this;
    }

    /**
     * 列車運行情報
     *
     * @return 検索結果のリスト
     * @throws URISyntaxException URI不正
     * @throws IOException        通信エラー
     */
    public List<TrainInformation> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<TrainInformation> trainInformations = new ArrayList<>();

        for (Map<String, Object> map : list) {
            trainInformations.add(TrainInformation.parse(map));
        }

        return trainInformations;
    }
}
