package tokyometro4j;

import tokyometro4j.common.Station;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 駅の順序を表すエンティティクラス.
 * <p/>
 * <table>
 * <tr>
 * <td>odpt:station</td>
 * <td>odpt:Station</td>
 * <td>駅</td>
 * </tr>
 * <tr>
 * <td>odpt:index</td>
 * <td>xsd:integer</td>
 * <td>駅の順序</td>
 * </tr>
 * </table>
 *
 * @see tokyometro4j.common.Station
 */
public class StationOrder {

    protected Station station;
    protected int index;

    public static StationOrder parse(final Map<String, Object> m) {
        StationOrder stationOrder = new StationOrder();

        stationOrder.station = Station.getStation(JsonUtil.getJsonString(m, "odpt:station"));
        stationOrder.index = JsonUtil.getJsonInt(m, "odpt:index");

        return stationOrder;
    }

    public Station getStation() {
        return station;
    }

    public int getIndex() {
        return index;
    }
}
