package tokyometro4j;

import tokyometro4j.entity.PassengerSurvey;
import tokyometro4j.entity.Railway;
import tokyometro4j.entity.RailwayFare;
import tokyometro4j.entity.Station;
import tokyometro4j.entity.StationFacility;
import tokyometro4j.entity.StationTimetable;
import tokyometro4j.entity.Train;
import tokyometro4j.entity.TrainInformation;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 * 各レスポンスの共通部分.
 * <p/>
 * <table>
 * <tr>
 * <td>@context</td>
 * <td>URL</td>
 * <td>JSON-LD仕様に基づく</td>
 * </tr>
 * <tr>
 * <td>@id</td>
 * <td>URN</td>
 * <td>固有識別子(ucode)。支線には別IDを割り当てる</td>
 * </tr>
 * <tr>
 * <td>@type</td>
 * <td></td>
 * <td></td>
 * </tr>
 * </table>
 */
public class SearchResponse {

    protected String context;
    protected String id;
    protected String type;

    public String getContext() {
        return context;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public static SearchResponse parse(final Map<String, Object> json) {
        String type = JsonUtil.getJsonString(json, "@type");
        SearchType.Type searchType = SearchType.Type.getInstance(type);

        switch (searchType) {
            case STATION_TIMETABLE:
                return StationTimetable.parse(json);
            case TRAIN_INFORMATION:
                return TrainInformation.parse(json);
            case TRAIN:
                return Train.parse(json);
            case STATION:
            case STATION_TMP:
                return Station.parse(json);
            case STATION_FACILITY:
                return  StationFacility.parse(json);
            case PASSENGER_SURVEY:
                return PassengerSurvey.parse(json);
            case RAILWAY:
                return Railway.parse(json);
            case RAILWAY_FARE:
                return  RailwayFare.parse(json);
        }
        return null;
    }

}
