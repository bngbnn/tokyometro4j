package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.Operator;
import tokyometro4j.entity.PassengerSurvey;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 駅乗降人員数情報の検索を行うクラスです
 */
public class PassengerSurveySearchType extends SearchType {

    /**
     * 検索API
     */
    protected final SearchApi searchApi;

    /**
     * 検索パラメーター
     */
    protected List<NameValuePair> params = new ArrayList<>();

    /**
     * 検索タイプ
     */
    private String type = "odpt:PassengerSurvey";

    /**
     * コンストラクタ
     *
     * @param searchApi 検索API
     */
    public PassengerSurveySearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    /**
     * 固有識別子の検索条件を設定します
     * <p>
     * 命名ルールはodpt.PassengerSurvey:TokyoMetro.駅名.調査年
     *
     * @param sameas 固有識別子
     * @return インスタンス
     */
    public PassengerSurveySearchType sameAs(String sameas) {
        this.params.add(new BasicNameValuePair("owl:sameAs", sameas));
        return this;
    }

    /**
     * 運行会社の検索条件を設定します
     *
     * @param operator 運行会社
     * @return インスタンス
     */
    public PassengerSurveySearchType operator(Operator operator) {
        this.params.add(new BasicNameValuePair("odpt:operator", operator.getValue()));
        return this;
    }

    /**
     * 調査年度の検索条件を設定します
     *
     * @param year 調査年度
     * @return インスタンス
     */
    public PassengerSurveySearchType surveyYear(int year) {
        this.params.add(new BasicNameValuePair("odpt:surveyYear", String.valueOf(year)));
        return this;
    }

    /**
     * 列車運行情報
     *
     * @return 検索結果のリスト
     * @throws java.net.URISyntaxException URI不正
     * @throws java.io.IOException         通信エラー
     */
    public List<PassengerSurvey> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<PassengerSurvey> passengerSurveys = new ArrayList<>();

        for (Map<String, Object> map : list) {
            passengerSurveys.add(PassengerSurvey.parse(map));
        }

        return passengerSurveys;
    }
}
