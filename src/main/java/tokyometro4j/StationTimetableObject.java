package tokyometro4j;

import tokyometro4j.common.TrainType;
import tokyometro4j.util.JsonUtil;

import java.util.Map;

/**
 *
 */
public class StationTimetableObject {

    protected String departureTime;
    protected String destinationStation;
    protected TrainType trainType;
    protected boolean isLast;
    protected boolean isOrigin;
    protected int carComposition;
    protected String note;

    public static StationTimetableObject parse(final Map map) {
        StationTimetableObject s = new StationTimetableObject();
        s.departureTime = JsonUtil.getJsonString(map, "odpt:departureTime");
        s.destinationStation = JsonUtil.getJsonString(map, "odpt:destinationStation");
        s.trainType = TrainType.getTrainType(JsonUtil.getJsonString(map, "odpt:trainType"));
        s.isLast = JsonUtil.getJsonBoolean(map, "odpt:isLast");
        s.isOrigin = JsonUtil.getJsonBoolean(map, "odpt:isOrigin");
        s.carComposition = JsonUtil.getJsonInt(map, "odpt:carComposition");
        s.note = JsonUtil.getJsonString(map, "odpt:note");

        return s;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public boolean isLast() {
        return isLast;
    }

    public boolean isOrigin() {
        return isOrigin;
    }

    public int getCarComposition() {
        return carComposition;
    }

    public String getNote() {
        return note;
    }
}
