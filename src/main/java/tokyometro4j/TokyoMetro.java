package tokyometro4j;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.ResourceBundle;

/**
 * Entry point for TokyoMetro REST API
 */
public class TokyoMetro {

    protected final String consumerKey;

    public TokyoMetro() {
        ResourceBundle rb = ResourceBundle.getBundle("tokyometro4j");
        consumerKey = rb.getString("consumerKey");
    }

    public TokyoMetro(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public DatapointsSearch datapoints() {
        return new DatapointsSearch(this);
    }

    public PlacesSearch places() {
        return new PlacesSearch(this);
    }

    public URIBuilder createUriBuilder() throws URISyntaxException {
        return new URIBuilder().setScheme("https").setHost("api.tokyometroapp.jp")
                .setPath("/api/v2/")
                .setParameter("acl:consumerKey", consumerKey);
    }
}
