package tokyometro4j;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;

/**
 *
 */
public class PlacesSearch extends SearchApi {

    protected static String path = "places";

    public PlacesSearch(TokyoMetro tokyoMetro) {
        this.tokyoMetro = tokyoMetro;
    }

    public PlacesStationSearchType station() {
        return new PlacesStationSearchType(this);
    }

    public PlacesRailwaySearchType railway() {
        return new PlacesRailwaySearchType(this);
    }

    /**
     * 検索用URIをかえします
     *
     * @return URI
     * @throws URISyntaxException パラメータ不正など
     */
    @Override
    public URIBuilder createUriBuilder() throws URISyntaxException {
        URIBuilder uriBuilder = tokyoMetro.createUriBuilder();
        return uriBuilder.setPath(uriBuilder.getPath() + path);
    }

    @Override
    public String getPath() {
        return path;
    }
}
