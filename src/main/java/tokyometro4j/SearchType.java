package tokyometro4j;

import tokyometro4j.util.HttpUtil;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class SearchType {

    public enum Type {
        STATION_TIMETABLE("odpt:StationTimetable"),
        TRAIN_INFORMATION("odpt:TrainInformation"),
        TRAIN("odpt:Train"),
        STATION("odpt:Station"),
        STATION_TMP("odpt.Station"), // TODO APIなおったら削除しましょう
        STATION_FACILITY("odpt:StationFacility"),
        PASSENGER_SURVEY("odpt:PassengerSurvey"),
        RAILWAY("odpt:Railway"),
        RAILWAY_FARE("odpt:RailwayFare"),;

        private final String value;

        public String getValue() {
            return value;
        }

        Type(final String value) {
            this.value = value;
        }

        public static Type getInstance(String t) {
            for (Type type : Type.values()) {
                if (type.getValue().equals(t)) {
                    return type;
                }
            }
            return null;
        }
    }


    protected List<Map<String, Object>> getApiResponse(final URI uri) throws IOException {
        return HttpUtil.getApiResponse(uri);
    }
}
