package tokyometro4j.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum RailDirection {
    ASAKUSA("odpt.RailDirection:TokyoMetro.Asakusa", "浅草方面"),
    OGIKUBO("odpt.RailDirection:TokyoMetro.Ogikubo", "荻窪方面"),
    IKEBUKURO("odpt.RailDirection:TokyoMetro.Ikebukuro", "池袋方面"),
    HONANCHO("odpt.RailDirection:TokyoMetro.Honancho", "方南町方面"),
    NAKANOSAKAUE("odpt.RailDirection:TokyoMetro.NakanoSakaue", "中野坂上方面"),
    NAKAMEGURO("odpt.RailDirection:TokyoMetro.NakaMeguro", "中目黒方面"),
    KITASENJU("odpt.RailDirection:TokyoMetro.KitaSenju", "北千住方面"),
    NISHIFUNABASHI("odpt.RailDirection:TokyoMetro.NishiFunabashi", "西船橋方面"),
    NAKANO("odpt.RailDirection:TokyoMetro.Nakano", "中野方面"),
    YOYOGIUEHARA("odpt.RailDirection:TokyoMetro.YoyogiUehara", "代々木上原方面"),
    AYASE("odpt.RailDirection:TokyoMetro.Ayase", "綾瀬方面"),
    KITAAYASE("odpt.RailDirection:TokyoMetro.KitaAyase", "北綾瀬方面"),
    SHINKIBA("odpt.RailDirection:TokyoMetro.ShinKiba", "新木場方面"),
    OSHIAGE("odpt.RailDirection:TokyoMetro.Oshiage", "押上方面"),
    SHIBUYA("odpt.RailDirection:TokyoMetro.Shibuya", "渋谷方面"),
    AKABANEIWABUCHI("odpt.RailDirection:TokyoMetro.AkabaneIwabuchi", "赤羽岩淵方面"),
    MEGURO("odpt.RailDirection:TokyoMetro.Meguro", "目黒方面"),
    SHIROKANETAKANAWA("odpt.RailDirection:TokyoMetro.ShirokaneTakanawa", "白金高輪方面"),
    WAKOSHI("odpt.RailDirection:TokyoMetro.Wakoshi", "和光市方面"),
    KOTAKEMUKAIHARA("odpt.RailDirection:TokyoMetro.KotakeMukaihara", "小竹向原方面"),;

    private static final Map<String, RailDirection> maps = new HashMap<>();

    static {
        for (RailDirection railDirection : values()) {
            maps.put(railDirection.value, railDirection);
        }
    }

    private final String value;

    private final String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    RailDirection(final String value, final String name) {
        this.value = value;
        this.name = name;
    }

    public static RailDirection getRailDirection(String value) {
        return maps.get(value);
    }

}
