package tokyometro4j.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum Operator {
    TOKYOMETRO("odpt.Operator:TokyoMetro", "東京メトロ");

    private static final Map<String, Operator> maps = new HashMap<>();

    static {
        for (Operator operator : values()) {
            maps.put(operator.value, operator);
        }
    }

    private final String value;

    private final String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    Operator(final String value, final String name) {
        this.value = value;
        this.name = name;
    }

    public static Operator getOperator(String value) {
        return maps.get(value);
    }

}
