package tokyometro4j.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum TrainOwner {
    TOKYOMETRO("odpt.TrainOwner:TokyoMetro", "東京メトロ"),
    SEIBU("odpt.TrainOwner:Seibu", "西武鉄道"),
    SAITAMARAILWAY("odpt.TrainOwner:SaitamaRailway", "埼玉高速鉄道"),
    TOBU("odpt.TrainOwner:Tobu", "東武鉄道"),
    TOYORAPIDRAILWAY("odpt.TrainOwner:ToyoRapidRailway", "東葉高速鉄道"),
    TOEI("odpt.TrainOwner:Toei", "都営地下鉄"),
    TOKYU("odpt.TrainOwner:Tokyu", "東急電鉄"),
    JR_EAST("odpt.TrainOwner:JR-East", "JR東日本"),
    ODAKYU("odpt.TrainOwner:Odakyu", "小田急電鉄"),;

    private static final Map<String, TrainOwner> maps = new HashMap<>();

    static {
        for (TrainOwner trainOwner : values()) {
            maps.put(trainOwner.value, trainOwner);
        }
    }

    private final String value;

    private final String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    TrainOwner(final String value, final String name) {
        this.value = value;
        this.name = name;
    }

    public static TrainOwner getTrainOwner(String value) {
        return maps.get(value);
    }
}
