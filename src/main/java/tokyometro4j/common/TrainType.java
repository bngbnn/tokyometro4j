package tokyometro4j.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum TrainType {
    UNKNOWN("odpt.TrainType:TokyoMetro.Unknown", "不明"),
    LOCAL("odpt.TrainType:TokyoMetro.Local", "各停"),
    EXPRESS("odpt.TrainType:TokyoMetro.Express", "急行"),
    RAPID("odpt.TrainType:TokyoMetro.Rapid", "快速"),
    SEMIEXPRESS("odpt.TrainType:TokyoMetro.SemiExpress", "準急"),
    TAMAEXPRESS("odpt.TrainType:TokyoMetro.TamaExpress", "多摩急行"),
    HOLIDAYEXPRESS("odpt.TrainType:TokyoMetro.HolidayExpress", "土休急行"),
    COMMUTERSEMIEXPRESS("odpt.TrainType:TokyoMetro.CommuterSemiExpress", "通勤準急"),
    EXTRA("odpt.TrainType:TokyoMetro.Extra", "臨時"),
    ROMANCECAR("odpt.TrainType:TokyoMetro.RomanceCar", "特急ロマンスカー"),
    RAPIDEXPRESS("odpt.TrainType:TokyoMetro.RapidExpress", "快速急行"),
    COMMUTEREXPRESS("odpt.TrainType:TokyoMetro.CommuterExpress", "通勤急行"),
    LIMITEDEXPRESS("odpt.TrainType:TokyoMetro.LimitedExpress", "特急"),
    COMMUTERLIMITEDEXPRESS("odpt.TrainType:TokyoMetro.CommuterLimitedExpress", "通勤特急"),
    COMMUTERRAPID("odpt.TrainType:TokyoMetro.CommuterRapid", "通勤快速"),
    TOYORAPID("odpt.TrainType:TokyoMetro.ToyoRapid", "東葉快速"),;

    private static final Map<String, TrainType> maps = new HashMap<>();

    static {
        for (TrainType trainType : values()) {
            maps.put(trainType.value, trainType);
        }
    }

    private final String value;

    private final String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    TrainType(final String value, final String name) {
        this.value = value;
        this.name = name;
    }

    public static TrainType getTrainType(String value) {
        return maps.get(value);
    }

}
