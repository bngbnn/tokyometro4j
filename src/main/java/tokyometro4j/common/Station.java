package tokyometro4j.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum Station {
    GINZA_AOYAMA_ITCHOME("G04", "odpt.Station:TokyoMetro.Ginza.AoyamaItchome", "青山一丁目"),
    HANZOMON_AOYAMA_ITCHOME("Z03", "odpt.Station:TokyoMetro.Hanzomon.AoyamaItchome", "青山一丁目"),
    CHIYODA_AKASAKA("C06", "odpt.Station:TokyoMetro.Chiyoda.Akasaka", "赤坂"),
    CHIYODA_AYASE("C19", "odpt.Station:TokyoMetro.Chiyoda.Ayase", "綾瀬"),
    GINZA_AKASAKA_MITSUKE("G05", "odpt.Station:TokyoMetro.Ginza.AkasakaMitsuke", "赤坂見附"),
    MARUNOUCHI_AKASAKA_MITSUKE("M13", "odpt.Station:TokyoMetro.Marunouchi.AkasakaMitsuke", "赤坂見附"),
    NAMBOKU_AKABANE_IWABUCHI("N19", "odpt.Station:TokyoMetro.Namboku.AkabaneIwabuchi", "赤羽岩淵"),
    HIBIYA_AKIHABARA("H15", "odpt.Station:TokyoMetro.Hibiya.Akihabara", "秋葉原"),
    GINZA_ASAKUSA("G19", "odpt.Station:TokyoMetro.Ginza.Asakusa", "浅草"),
    NAMBOKU_AZABU_JUBAN("N04", "odpt.Station:TokyoMetro.Namboku.AzabuJuban", "麻布十番"),
    MARUNOUCHI_AWAJICHO("M19", "odpt.Station:TokyoMetro.Marunouchi.Awajicho", "淡路町"),
    TOZAI_IIDABASHI("T06", "odpt.Station:TokyoMetro.Tozai.Iidabashi", "飯田橋"),
    YURAKUCHO_IIDABASHI("Y13", "odpt.Station:TokyoMetro.Yurakucho.Iidabashi", "飯田橋"),
    NAMBOKU_IIDABASHI("N10", "odpt.Station:TokyoMetro.Namboku.Iidabashi", "飯田橋"),
    MARUNOUCHI_IKEBUKURO("M25", "odpt.Station:TokyoMetro.Marunouchi.Ikebukuro", "池袋"),
    YURAKUCHO_ICHIGAYA("Y14", "odpt.Station:TokyoMetro.Yurakucho.Ichigaya", "市ケ谷"),
    GINZA_INARICHO("G17", "odpt.Station:TokyoMetro.Ginza.Inaricho", "稲荷町"),
    YURAKUCHO_IKEBUKURO("Y09", "odpt.Station:TokyoMetro.Yurakucho.Ikebukuro", "池袋"),
    FUKUTOSHIN_IKEBUKURO("F09", "odpt.Station:TokyoMetro.Fukutoshin.Ikebukuro", "池袋"),
    NAMBOKU_ICHIGAYA("N09", "odpt.Station:TokyoMetro.Namboku.Ichigaya", "市ケ谷"),
    HIBIYA_IRIYA("H18", "odpt.Station:TokyoMetro.Hibiya.Iriya", "入谷"),
    GINZA_UENO("G16", "odpt.Station:TokyoMetro.Ginza.Ueno", "上野"),
    HIBIYA_UENO("H17", "odpt.Station:TokyoMetro.Hibiya.Ueno", "上野"),
    GINZA_UENO_HIROKOJI("G15", "odpt.Station:TokyoMetro.Ginza.UenoHirokoji", "上野広小路"),
    TOZAI_URAYASU("T18", "odpt.Station:TokyoMetro.Tozai.Urayasu", "浦安"),
    YURAKUCHO_EDOGAWABASHI("Y12", "odpt.Station:TokyoMetro.Yurakucho.Edogawabashi", "江戸川橋"),
    HIBIYA_EBISU("H02", "odpt.Station:TokyoMetro.Hibiya.Ebisu", "恵比寿"),
    NAMBOKU_OJI("N16", "odpt.Station:TokyoMetro.Namboku.Oji", "王子"),
    NAMBOKU_OJI_KAMIYA("N17", "odpt.Station:TokyoMetro.Namboku.OjiKamiya", "王子神谷"),
    MARUNOUCHI_OTEMACHI("M18", "odpt.Station:TokyoMetro.Marunouchi.Otemachi", "大手町"),
    TOZAI_OTEMACHI("T09", "odpt.Station:TokyoMetro.Tozai.Otemachi", "大手町"),
    CHIYODA_OTEMACHI("C11", "odpt.Station:TokyoMetro.Chiyoda.Otemachi", "大手町"),
    HANZOMON_OTEMACHI("Z08", "odpt.Station:TokyoMetro.Hanzomon.Otemachi", "大手町"),
    MARUNOUCHI_OGIKUBO("M01", "odpt.Station:TokyoMetro.Marunouchi.Ogikubo", "荻窪"),
    HANZOMON_OSHIAGE("Z14", "odpt.Station:TokyoMetro.Hanzomon.Oshiage", "押上〈スカイツリー前〉"),
    TOZAI_OCHIAI("T02", "odpt.Station:TokyoMetro.Tozai.Ochiai", "落合"),
    MARUNOUCHI_OCHANOMIZU("M20", "odpt.Station:TokyoMetro.Marunouchi.Ochanomizu", "御茶ノ水"),
    GINZA_OMOTE_SANDO("G02", "odpt.Station:TokyoMetro.Ginza.OmoteSando", "表参道"),
    CHIYODA_OMOTE_SANDO("C04", "odpt.Station:TokyoMetro.Chiyoda.OmoteSando", "表参道"),
    HANZOMON_OMOTE_SANDO("Z02", "odpt.Station:TokyoMetro.Hanzomon.OmoteSando", "表参道"),
    GINZA_GAIEMMAE("G03", "odpt.Station:TokyoMetro.Ginza.Gaiemmae", "外苑前"),
    TOZAI_KAGURAZAKA("T05", "odpt.Station:TokyoMetro.Tozai.Kagurazaka", "神楽坂"),
    TOZAI_KASAI("T17", "odpt.Station:TokyoMetro.Tozai.Kasai", "葛西"),
    MARUNOUCHI_KASUMIGASEKI("M15", "odpt.Station:TokyoMetro.Marunouchi.Kasumigaseki", "霞ケ関"),
    HIBIYA_KASUMIGASEKI("H06", "odpt.Station:TokyoMetro.Hibiya.Kasumigaseki", "霞ケ関"),
    CHIYODA_KASUMIGASEKI("C08", "odpt.Station:TokyoMetro.Chiyoda.Kasumigaseki", "霞ケ関"),
    YURAKUCHO_KANAMECHO("Y08", "odpt.Station:TokyoMetro.Yurakucho.Kanamecho", "要町"),
    FUKUTOSHIN_KANAMECHO("F08", "odpt.Station:TokyoMetro.Fukutoshin.Kanamecho", "要町"),
    HIBIYA_KAMIYACHO("H05", "odpt.Station:TokyoMetro.Hibiya.Kamiyacho", "神谷町"),
    HIBIYA_KAYABACHO("H12", "odpt.Station:TokyoMetro.Hibiya.Kayabacho", "茅場町"),
    TOZAI_KAYABACHO("T11", "odpt.Station:TokyoMetro.Tozai.Kayabacho", "茅場町"),
    GINZA_KANDA("G13", "odpt.Station:TokyoMetro.Ginza.Kanda", "神田"),
    CHIYODA_KITA_AYASE("C20", "odpt.Station:TokyoMetro.Chiyoda.KitaAyase", "北綾瀬"),
    FUKUTOSHIN_KITA_SANDO("F14", "odpt.Station:TokyoMetro.Fukutoshin.KitaSando", "北参道"),
    HIBIYA_KITA_SENJU("H21", "odpt.Station:TokyoMetro.Hibiya.KitaSenju", "北千住"),
    CHIYODA_KITA_SENJU("C18", "odpt.Station:TokyoMetro.Chiyoda.KitaSenju", "北千住"),
    TOZAI_KIBA("T13", "odpt.Station:TokyoMetro.Tozai.Kiba", "木場"),
    TOZAI_GYOTOKU("T20", "odpt.Station:TokyoMetro.Tozai.Gyotoku", "行徳"),
    GINZA_KYOBASHI("G10", "odpt.Station:TokyoMetro.Ginza.Kyobashi", "京橋"),
    HANZOMON_KIYOSUMI_SHIRAKAWA("Z11", "odpt.Station:TokyoMetro.Hanzomon.KiyosumiShirakawa", "清澄白河"),
    GINZA_GINZA("G09", "odpt.Station:TokyoMetro.Ginza.Ginza", "銀座"),
    MARUNOUCHI_GINZA("M16", "odpt.Station:TokyoMetro.Marunouchi.Ginza", "銀座"),
    HIBIYA_GINZA("H08", "odpt.Station:TokyoMetro.Hibiya.Ginza", "銀座"),
    YURAKUCHO_GINZA_ITCHOME("Y19", "odpt.Station:TokyoMetro.Yurakucho.GinzaItchome", "銀座一丁目"),
    HANZOMON_KINSHICHO("Z13", "odpt.Station:TokyoMetro.Hanzomon.Kinshicho", "錦糸町"),
    TOZAI_KUDANSHITA("T07", "odpt.Station:TokyoMetro.Tozai.Kudanshita", "九段下"),
    HANZOMON_KUDANSHITA("Z06", "odpt.Station:TokyoMetro.Hanzomon.Kudanshita", "九段下"),
    YURAKUCHO_KOJIMACHI("Y15", "odpt.Station:TokyoMetro.Yurakucho.Kojimachi", "麴町"),
    MARUNOUCHI_KORAKUEN("M22", "odpt.Station:TokyoMetro.Marunouchi.Korakuen", "後楽園"),
    NAMBOKU_KORAKUEN("N11", "odpt.Station:TokyoMetro.Namboku.Korakuen", "後楽園"),
    YURAKUCHO_GOKOKUJI("Y11", "odpt.Station:TokyoMetro.Yurakucho.Gokokuji", "護国寺"),
    YURAKUCHO_KOTAKE_MUKAIHARA("Y06", "odpt.Station:TokyoMetro.Yurakucho.KotakeMukaihara", "小竹向原"),
    FUKUTOSHIN_KOTAKE_MUKAIHARA("F06", "odpt.Station:TokyoMetro.Fukutoshin.KotakeMukaihara", "小竹向原"),
    MARUNOUCHI_KOKKAI_GIJIDOMAE("M14", "odpt.Station:TokyoMetro.Marunouchi.KokkaiGijidomae", "国会議事堂前"),
    CHIYODA_KOKKAI_GIJIDOMAE("C07", "odpt.Station:TokyoMetro.Chiyoda.KokkaiGijidomae", "国会議事堂前"),
    HIBIYA_KODEMMACHO("H14", "odpt.Station:TokyoMetro.Hibiya.Kodemmacho", "小伝馬町"),
    NAMBOKU_KOMAGOME("N14", "odpt.Station:TokyoMetro.Namboku.Komagome", "駒込"),
    YURAKUCHO_SAKURADAMON("Y17", "odpt.Station:TokyoMetro.Yurakucho.Sakuradamon", "桜田門"),
    GINZA_SHIBUYA("G01", "odpt.Station:TokyoMetro.Ginza.Shibuya", "渋谷"),
    HANZOMON_SHIBUYA("Z01", "odpt.Station:TokyoMetro.Hanzomon.Shibuya", "渋谷"),
    FUKUTOSHIN_SHIBUYA("F16", "odpt.Station:TokyoMetro.Fukutoshin.Shibuya", "渋谷"),
    NAMBOKU_SHIMO("N18", "odpt.Station:TokyoMetro.Namboku.Shimo", "志茂"),
    NAMBOKU_SHIROKANEDAI("N02", "odpt.Station:TokyoMetro.Namboku.Shirokanedai", "白金台"),
    NAMBOKU_SHIROKANE_TAKANAWA("N03", "odpt.Station:TokyoMetro.Namboku.ShirokaneTakanawa", "白金高輪"),
    MARUNOUCHI_SHIN_OTSUKA("M24", "odpt.Station:TokyoMetro.Marunouchi.ShinOtsuka", "新大塚"),
    CHIYODA_SHIN_OCHANOMIZU("C12", "odpt.Station:TokyoMetro.Chiyoda.ShinOchanomizu", "新御茶ノ水"),
    YURAKUCHO_SHIN_KIBA("Y24", "odpt.Station:TokyoMetro.Yurakucho.ShinKiba", "新木場"),
    MARUNOUCHI_SHIN_KOENJI("M03", "odpt.Station:TokyoMetro.Marunouchi.ShinKoenji", "新高円寺"),
    MARUNOUCHI_SHINJUKU("M08", "odpt.Station:TokyoMetro.Marunouchi.Shinjuku", "新宿"),
    MARUNOUCHI_SHINJUKU_GYOEMMAE("M10", "odpt.Station:TokyoMetro.Marunouchi.ShinjukuGyoemmae", "新宿御苑前"),
    MARUNOUCHI_SHINJUKU_SANCHOME("M09", "odpt.Station:TokyoMetro.Marunouchi.ShinjukuSanchome", "新宿三丁目"),
    FUKUTOSHIN_SHINJUKU_SANCHOME("F13", "odpt.Station:TokyoMetro.Fukutoshin.ShinjukuSanchome", "新宿三丁目"),
    YURAKUCHO_SHINTOMICHO("Y20", "odpt.Station:TokyoMetro.Yurakucho.Shintomicho", "新富町"),
    MARUNOUCHI_SHIN_NAKANO("M05", "odpt.Station:TokyoMetro.Marunouchi.ShinNakano", "新中野"),
    GINZA_SHIMBASHI("G08", "odpt.Station:TokyoMetro.Ginza.Shimbashi", "新橋"),
    HANZOMON_JIMBOCHO("Z07", "odpt.Station:TokyoMetro.Hanzomon.Jimbocho", "神保町"),
    HANZOMON_SUITENGUMAE("Z10", "odpt.Station:TokyoMetro.Hanzomon.Suitengumae", "水天宮前"),
    GINZA_SUEHIROCHO("G14", "odpt.Station:TokyoMetro.Ginza.Suehirocho", "末広町"),
    HANZOMON_SUMIYOSHI("Z12", "odpt.Station:TokyoMetro.Hanzomon.Sumiyoshi", "住吉"),
    YURAKUCHO_SENKAWA("Y07", "odpt.Station:TokyoMetro.Yurakucho.Senkawa", "千川"),
    FUKUTOSHIN_SENKAWA("F07", "odpt.Station:TokyoMetro.Fukutoshin.Senkawa", "千川"),
    CHIYODA_SENDAGI("C15", "odpt.Station:TokyoMetro.Chiyoda.Sendagi", "千駄木"),
    FUKUTOSHIN_ZOSHIGAYA("F10", "odpt.Station:TokyoMetro.Fukutoshin.Zoshigaya", "雑司が谷"),
    TOZAI_TAKADANOBABA("T03", "odpt.Station:TokyoMetro.Tozai.Takadanobaba", "高田馬場"),
    TOZAI_TAKEBASHI("T08", "odpt.Station:TokyoMetro.Tozai.Takebashi", "竹橋"),
    YURAKUCHO_TATSUMI("Y23", "odpt.Station:TokyoMetro.Yurakucho.Tatsumi", "辰巳"),
    GINZA_TAMEIKE_SANNO("G06", "odpt.Station:TokyoMetro.Ginza.TameikeSanno", "溜池山王"),
    NAMBOKU_TAMEIKE_SANNO("N06", "odpt.Station:TokyoMetro.Namboku.TameikeSanno", "溜池山王"),
    GINZA_TAWARAMACHI("G18", "odpt.Station:TokyoMetro.Ginza.Tawaramachi", "田原町"),
    YURAKUCHO_CHIKATETSU_AKATSUKA("Y03", "odpt.Station:TokyoMetro.Yurakucho.ChikatetsuAkatsuka", "地下鉄赤塚"),
    YURAKUCHO_CHIKATETSU_NARIMASU("Y02", "odpt.Station:TokyoMetro.Yurakucho.ChikatetsuNarimasu", "地下鉄成増"),
    HIBIYA_TSUKIJI("H10", "odpt.Station:TokyoMetro.Hibiya.Tsukiji", "築地"),
    YURAKUCHO_TSUKISHIMA("Y21", "odpt.Station:TokyoMetro.Yurakucho.Tsukishima", "月島"),
    MARUNOUCHI_TOKYO("M17", "odpt.Station:TokyoMetro.Marunouchi.Tokyo", "東京"),
    NAMBOKU_TODAIMAE("N12", "odpt.Station:TokyoMetro.Namboku.Todaimae", "東大前"),
    TOZAI_TOYOCHO("T14", "odpt.Station:TokyoMetro.Tozai.Toyocho", "東陽町"),
    YURAKUCHO_TOYOSU("Y22", "odpt.Station:TokyoMetro.Yurakucho.Toyosu", "豊洲"),
    GINZA_TORANOMON("G07", "odpt.Station:TokyoMetro.Ginza.Toranomon", "虎ノ門"),
    HIBIYA_NAKA_OKACHIMACHI("H16", "odpt.Station:TokyoMetro.Hibiya.NakaOkachimachi", "仲御徒町"),
    YURAKUCHO_NAGATACHO("Y16", "odpt.Station:TokyoMetro.Yurakucho.Nagatacho", "永田町"),
    HANZOMON_NAGATACHO("Z04", "odpt.Station:TokyoMetro.Hanzomon.Nagatacho", "永田町"),
    NAMBOKU_NAGATACHO("N07", "odpt.Station:TokyoMetro.Namboku.Nagatacho", "永田町"),
    TOZAI_NAKANO("T01", "odpt.Station:TokyoMetro.Tozai.Nakano", "中野"),
    MARUNOUCHI_NAKANO_SAKAUE("M06", "odpt.Station:TokyoMetro.Marunouchi.NakanoSakaue", "中野坂上"),
    MARUNOUCHIBRANCH_NAKANO_SHIMBASHI("m05", "odpt.Station:TokyoMetro.MarunouchiBranch.NakanoShimbashi", "中野新橋"),
    MARUNOUCHIBRANCH_NAKANO_FUJIMICHO("m04", "odpt.Station:TokyoMetro.MarunouchiBranch.NakanoFujimicho", "中野富士見町"),
    HIBIYA_NAKA_MEGURO("H01", "odpt.Station:TokyoMetro.Hibiya.NakaMeguro", "中目黒"),
    TOZAI_NISHI_KASAI("T16", "odpt.Station:TokyoMetro.Tozai.NishiKasai", "西葛西"),
    NAMBOKU_NISHIGAHARA("N15", "odpt.Station:TokyoMetro.Namboku.Nishigahara", "西ケ原"),
    MARUNOUCHI_NISHI_SHINJUKU("M07", "odpt.Station:TokyoMetro.Marunouchi.NishiShinjuku", "西新宿"),
    CHIYODA_NISHI_NIPPORI("C16", "odpt.Station:TokyoMetro.Chiyoda.NishiNippori", "西日暮里"),
    TOZAI_NISHI_FUNABASHI("T23", "odpt.Station:TokyoMetro.Tozai.NishiFunabashi", "西船橋"),
    FUKUTOSHIN_NISHI_WASEDA("F11", "odpt.Station:TokyoMetro.Fukutoshin.NishiWaseda", "西早稲田"),
    CHIYODA_NIJUBASHIMAE("C10", "odpt.Station:TokyoMetro.Chiyoda.Nijubashimae", "二重橋前"),
    GINZA_NIHOMBASHI("G11", "odpt.Station:TokyoMetro.Ginza.Nihombashi", "日本橋"),
    TOZAI_NIHOMBASHI("T10", "odpt.Station:TokyoMetro.Tozai.Nihombashi", "日本橋"),
    HIBIYA_NINGYOCHO("H13", "odpt.Station:TokyoMetro.Hibiya.Ningyocho", "人形町"),
    CHIYODA_NEZU("C14", "odpt.Station:TokyoMetro.Chiyoda.Nezu", "根津"),
    CHIYODA_NOGIZAKA("C05", "odpt.Station:TokyoMetro.Chiyoda.Nogizaka", "乃木坂"),
    HIBIYA_HATCHOBORI("H11", "odpt.Station:TokyoMetro.Hibiya.Hatchobori", "八丁堀"),
    TOZAI_BARAKI_NAKAYAMA("T22", "odpt.Station:TokyoMetro.Tozai.BarakiNakayama", "原木中山"),
    HANZOMON_HANZOMON("Z05", "odpt.Station:TokyoMetro.Hanzomon.Hanzomon", "半蔵門"),
    YURAKUCHO_HIGASHI_IKEBUKURO("Y10", "odpt.Station:TokyoMetro.Yurakucho.HigashiIkebukuro", "東池袋"),
    HIBIYA_HIGASHI_GINZA("H09", "odpt.Station:TokyoMetro.Hibiya.HigashiGinza", "東銀座"),
    MARUNOUCHI_HIGASHI_KOENJI("M04", "odpt.Station:TokyoMetro.Marunouchi.HigashiKoenji", "東高円寺"),
    FUKUTOSHIN_HIGASHI_SHINJUKU("F12", "odpt.Station:TokyoMetro.Fukutoshin.HigashiShinjuku", "東新宿"),
    YURAKUCHO_HIKAWADAI("Y05", "odpt.Station:TokyoMetro.Yurakucho.Hikawadai", "氷川台"),
    HIBIYA_HIBIYA("H07", "odpt.Station:TokyoMetro.Hibiya.Hibiya", "日比谷"),
    CHIYODA_HIBIYA("C09", "odpt.Station:TokyoMetro.Chiyoda.Hibiya", "日比谷"),
    HIBIYA_HIRO_O("H03", "odpt.Station:TokyoMetro.Hibiya.HiroO", "広尾"),
    YURAKUCHO_HEIWADAI("Y04", "odpt.Station:TokyoMetro.Yurakucho.Heiwadai", "平和台"),
    MARUNOUCHIBRANCH_HONANCHO("m03", "odpt.Station:TokyoMetro.MarunouchiBranch.Honancho", "方南町"),
    MARUNOUCHI_HONGO_SANCHOME("M21", "odpt.Station:TokyoMetro.Marunouchi.HongoSanchome", "本郷三丁目"),
    NAMBOKU_HON_KOMAGOME("N13", "odpt.Station:TokyoMetro.Namboku.HonKomagome", "本駒込"),
    CHIYODA_MACHIYA("C17", "odpt.Station:TokyoMetro.Chiyoda.Machiya", "町屋"),
    GINZA_MITSUKOSHIMAE("G12", "odpt.Station:TokyoMetro.Ginza.Mitsukoshimae", "三越前"),
    HANZOMON_MITSUKOSHIMAE("Z09", "odpt.Station:TokyoMetro.Hanzomon.Mitsukoshimae", "三越前"),
    MARUNOUCHI_MINAMI_ASAGAYA("M02", "odpt.Station:TokyoMetro.Marunouchi.MinamiAsagaya", "南阿佐ケ谷"),
    TOZAI_MINAMI_GYOTOKU("T19", "odpt.Station:TokyoMetro.Tozai.MinamiGyotoku", "南行徳"),
    TOZAI_MINAMI_SUNAMACHI("T15", "odpt.Station:TokyoMetro.Tozai.MinamiSunamachi", "南砂町"),
    HIBIYA_MINAMI_SENJU("H20", "odpt.Station:TokyoMetro.Hibiya.MinamiSenju", "南千住"),
    HIBIYA_MINOWA("H19", "odpt.Station:TokyoMetro.Hibiya.Minowa", "三ノ輪"),
    MARUNOUCHI_MYOGADANI("M23", "odpt.Station:TokyoMetro.Marunouchi.Myogadani", "茗荷谷"),
    TOZAI_MYODEN("T21", "odpt.Station:TokyoMetro.Tozai.Myoden", "妙典"),
    CHIYODA_MEIJI_JINGUMAE("C03", "odpt.Station:TokyoMetro.Chiyoda.MeijiJingumae", "明治神宮前〈原宿〉"),
    FUKUTOSHIN_MEIJI_JINGUMAE("F15", "odpt.Station:TokyoMetro.Fukutoshin.MeijiJingumae", "明治神宮前〈原宿〉"),
    NAMBOKU_MEGURO("N01", "odpt.Station:TokyoMetro.Namboku.Meguro", "目黒"),
    TOZAI_MONZEN_NAKACHO("T12", "odpt.Station:TokyoMetro.Tozai.MonzenNakacho", "門前仲町"),
    YURAKUCHO_YURAKUCHO("Y18", "odpt.Station:TokyoMetro.Yurakucho.Yurakucho", "有楽町"),
    CHIYODA_YUSHIMA("C13", "odpt.Station:TokyoMetro.Chiyoda.Yushima", "湯島"),
    MARUNOUCHI_YOTSUYA("M12", "odpt.Station:TokyoMetro.Marunouchi.Yotsuya", "四ツ谷"),
    NAMBOKU_YOTSUYA("N08", "odpt.Station:TokyoMetro.Namboku.Yotsuya", "四ツ谷"),
    MARUNOUCHI_YOTSUYA_SANCHOME("M11", "odpt.Station:TokyoMetro.Marunouchi.YotsuyaSanchome", "四谷三丁目"),
    CHIYODA_YOYOGI_UEHARA("C01", "odpt.Station:TokyoMetro.Chiyoda.YoyogiUehara", "代々木上原"),
    CHIYODA_YOYOGI_KOEN("C02", "odpt.Station:TokyoMetro.Chiyoda.YoyogiKoen", "代々木公園"),
    HIBIYA_ROPPONGI("H04", "odpt.Station:TokyoMetro.Hibiya.Roppongi", "六本木"),
    NAMBOKU_ROPPONGI_ITCHOME("N05", "odpt.Station:TokyoMetro.Namboku.RoppongiItchome", "六本木一丁目"),
    YURAKUCHO_WAKOSHI("Y01", "odpt.Station:TokyoMetro.Yurakucho.Wakoshi", "和光市"),
    TOZAI_WASEDA("T04", "odpt.Station:TokyoMetro.Tozai.Waseda", "早稲田"),
    MARUNOUCHIBRANCH_NAKANO_SAKAUE("M06", "odpt.Station:TokyoMetro.MarunouchiBranch.NakanoSakaue", "中野坂上"),
    FUKUTOSHIN_HIKAWADAI("F05", "odpt.Station:TokyoMetro.Fukutoshin.Hikawadai", "氷川台"),
    FUKUTOSHIN_HEIWADAI("F04", "odpt.Station:TokyoMetro.Fukutoshin.Heiwadai", "平和台"),
    FUKUTOSHIN_CHIKATETSU_AKATSUKA("F03", "odpt.Station:TokyoMetro.Fukutoshin.ChikatetsuAkatsuka", "地下鉄赤塚"),
    FUKUTOSHIN_CHIKATETSU_NARIMASU("F02", "odpt.Station:TokyoMetro.Fukutoshin.ChikatetsuNarimasu", "地下鉄成増"),
    FUKUTOSHIN_WAKOSHI("F01", "odpt.Station:TokyoMetro.Fukutoshin.Wakoshi", "和光市"),

    //FROM TokyoMetro WebSite
    JR_EAST_JOBAN_ABIKO("", "odpt.Station:JR-East.Joban.Abiko", "我孫子"),
    JR_EAST_JOBAN_TORIDE("", "odpt.Station:JR-East.Joban.Toride", "取手"),
    JR_EAST_JOBAN_KASHIWA("", "odpt.Station:JR-East.Joban.Kashiwa", "柏"),
    JR_EAST_JOBAN_MATSUDO("", "odpt.Station:JR-East.Joban.Matsudo", "松戸"),
    JR_EAST_CHUO_MITAKA("", "odpt.Station:JR-East.Chuo.Mitaka", "三鷹"),
    TOEI_MITA_MITA("", "odpt.Station:Toei.Mita.Mita", "三田"),
    TOEI_MITA_SHIBAKOEN("", "odpt.Station:Toei.Mita.Shibakoen", "芝公園"),
    TOEI_MITA_ONARIMON("", "odpt.Station:Toei.Mita.Onarimon", "御成門"),
    TOEI_MITA_UCHISAIWAICHO("", "odpt.Station:Toei.Mita.Uchisaiwaicho", "内幸町"),
    TOEI_MITA_HIBIYA("", "odpt.Station:Toei.Mita.Hibiya", "日比谷"),
    TOEI_MITA_OTEMACHI("", "odpt.Station:Toei.Mita.Otemachi", "大手町"),
    TOEI_MITA_JIMBOCHO("", "odpt.Station:Toei.Mita.Jimbocho", "神保町"),
    TOEI_MITA_SUIDOBASHI("", "odpt.Station:Toei.Mita.Suidobashi", "水道橋"),
    TOEI_MITA_KASUGA("", "odpt.Station:Toei.Mita.Kasuga", "春日"),
    TOEI_MITA_HAKUSAN("", "odpt.Station:Toei.Mita.Hakusan", "白山"),
    TOEI_MITA_SENGOKU("", "odpt.Station:Toei.Mita.Sengoku", "千石"),
    TOEI_MITA_SUGAMO("", "odpt.Station:Toei.Mita.Sugamo", "巣鴨"),
    TOEI_MITA_NISHISUGAMO("", "odpt.Station:Toei.Mita.NishiSugamo", "西巣鴨"),
    TOEI_MITA_SHINITABASHI("", "odpt.Station:Toei.Mita.ShinItabashi", "新板橋"),
    TOEI_MITA_ITABASHIKUYAKUSHOMAE("", "odpt.Station:Toei.Mita.Itabashikuyakushomae", "板橋区役所前"),
    TOEI_MITA_ITABASHIHONCHO("", "odpt.Station:Toei.Mita.Itabashihoncho", "板橋本町"),
    TOEI_MITA_MOTOHASUNUMA("", "odpt.Station:Toei.Mita.Motohasunuma", "本蓮沼"),
    TOEI_MITA_SHIMURASANCHOME("", "odpt.Station:Toei.Mita.ShimuraSanchome", "志村坂上"),
    TOEI_MITA_HASUNE("", "odpt.Station:Toei.Mita.Hasune", "蓮根"),
    TOEI_MITA_NISHIDAI("", "odpt.Station:Toei.Mita.Nishidai", "西台"),
    TOEI_MITA_TAKASHIMADAIRA("", "odpt.Station:Toei.Mita.Takashimadaira", "高島平"),
    TOEI_MITA_SHINTAKASHIMADAIRA("", "odpt.Station:Toei.Mita.ShinTakashimadaira", "新高島平"),
    TOEI_MITA_NISHITAKASHIMADAIRA("", "odpt.Station:Toei.Mita.NishiTakashimadaira", "西高島平"),
    SAITAMARAILWAY_SAITAMARAILWAY_URAWAMISONO("", "odpt.Station:SaitamaRailway.SaitamaRailway.UrawaMisono", "浦和美園"),
    TOYORAPIDRAILWAY_TOYORAPID_TOYOKATSUTADAI("", "odpt.Station:ToyoRapidRailway.ToyoRapid.ToyoKatsutadai", "東葉勝田台"),
    ODAKYU_TAMA_KARAKIDA("", "odpt.Station:Odakyu.Tama.Karakida", "唐木田"),
    TOBU_NIKKO_MINAMIKURIHASHI("", "odpt.Station:Tobu.Nikko.MinamiKurihashi", "南栗橋"),
    TOBU_ISESAKI_KUKI("", "odpt.Station:Tobu.Isesaki.Kuki", "久喜 　"),
    TOBU_ISESAKI_KITAKOSHIGAYA("", "odpt.Station:Tobu.Isesaki.KitaKoshigaya", "北越谷"),
    TOBU_ISESAKI_TOBUDOUBUTUKOUEN("", "odpt.Station:Tobu.Isesaki.TobuDoubutuKouen", "東武動物公園"),
    TOBU_TOJO_KAWAGOESHI("", "odpt.Station:Tobu.Tojo.Kawagoeshi", "川越市"),
    TOBU_TOJO_ASAKA("", "odpt.Station:Tobu.Tojo.Asaka", "朝霧"),
    TOBU_TOJO_ASAKADAI("", "odpt.Station:Tobu.Tojo.Asakadai", "朝霧台"),
    TOBU_TOJO_SHIKI("", "odpt.Station:Tobu.Tojo.Shiki", "志木"),
    TOBU_TOJO_YANASEGAWA("", "odpt.Station:Tobu.Tojo.Yanasegawa", "柳瀬川"),
    TOBU_TOJO_MIZUHODAI("", "odpt.Station:Tobu.Tojo.Mizuhodai", "みずほ台"),
    TOBU_TOJO_TSURUSE("", "odpt.Station:Tobu.Tojo.Tsuruse", "鶴瀬"),
    TOBU_TOJO_FUJIMINO("", "odpt.Station:Tobu.Tojo.Fujimino", "ふじみ野"),
    TOBU_TOJO_KAMIFUKUOKA("", "odpt.Station:Tobu.Tojo.KamiFukuoka", "上福岡"),
    TOBU_TOJO_SHINGASHI("", "odpt.Station:Tobu.Tojo.Shingashi", "新河岸"),
    TOBU_TOJO_KAWAGOE("", "odpt.Station:Tobu.Tojo.Kawagoe", "川越"),
    TOBU_TOJO_KASUMIGASEKI("", "odpt.Station:Tobu.Tojo.Kasumigaseki", "霞ヶ関"),
    TOBU_TOJO_TSURUGASHIMA("", "odpt.Station:Tobu.Tojo.Tsurugashima", "鶴ヶ島"),
    TOBU_TOJO_WAKABA("", "odpt.Station:Tobu.Tojo.Wakaba", "若葉"),
    TOBU_TOJO_SAKADO("", "odpt.Station:Tobu.Tojo.Sakado", "坂戸"),
    TOBU_TOJO_KITASAKADO("", "odpt.Station:Tobu.Tojo.KitaSakado", "北坂戸"),
    TOBU_TOJO_TAKASAKA("", "odpt.Station:Tobu.Tojo.Takasaka", "高坂"),
    TOBU_TOJO_HIGASHIMATSUYAMA("", "odpt.Station:Tobu.Tojo.HigashiMatsuyama", "東松山"),
    TOBU_TOJO_SHINRINKOEN("", "odpt.Station:Tobu.Tojo.ShinrinKoen", "森林公園"),
    TOKYU_DENENTOSHI_CHUORINKAN("", "odpt.Station:Tokyu.DenEnToshi.ChuoRinkan", "中央林間"),
    TOKYU_TOYOKO_HIYOSHI("", "odpt.Station:Tokyu.Toyoko.Hiyoshi", "日吉"),
    MINATOMIRAI_MINATOMIRAI_MOTOMACHICHUKAGAI("", "odpt.Station:Minatomirai.Minatomirai.MotomachiChukagai", "元町・中華街"),
    SEIBU_IKEBUKURO_SHINSAKURADAI("", "odpt.Station:Seibu.Ikebukuro.ShinSakuradai", "新桜台"),
    SEIBU_IKEBUKURO_NERIMA("", "odpt.Station:Seibu.Ikebukuro.Nerima", "練馬"),
    SEIBU_IKEBUKURO_NAKAMURABASHI("", "odpt.Station:Seibu.Ikebukuro.Nakamurabashi", "中村橋"),
    SEIBU_IKEBUKURO_FUJIMIDAI("", "odpt.Station:Seibu.Ikebukuro.Fujimidai", "富士見台"),
    SEIBU_IKEBUKURO_NERIMATAKANODAI("", "odpt.Station:Seibu.Ikebukuro.NerimaTakanodai", "練馬高野台"),
    SEIBU_IKEBUKURO_SHAKUJIIKOEN("", "odpt.Station:Seibu.Ikebukuro.ShakujiiKoen", "石神井公園"),
    SEIBU_IKEBUKURO_OIZUMIGAKUEN("", "odpt.Station:Seibu.Ikebukuro.OizumiGakuen", "大泉学園"),
    SEIBU_IKEBUKURO_HOYA("", "odpt.Station:Seibu.Ikebukuro.Hoya", "保谷"),
    SEIBU_IKEBUKURO_HIBARIGAOKA("", "odpt.Station:Seibu.Ikebukuro.Hibarigaoka", "ひばりヶ丘"),
    SEIBU_IKEBUKURO_HIGASHIKURUME("", "odpt.Station:Seibu.Ikebukuro.HigashiKurume", "東久留米"),
    SEIBU_IKEBUKURO_KIYOSE("", "odpt.Station:Seibu.Ikebukuro.Kiyose", "清瀬"),
    SEIBU_IKEBUKURO_AKITSU("", "odpt.Station:Seibu.Ikebukuro.Akitsu", "秋津"),
    SEIBU_IKEBUKURO_TOKOROZAWA("", "odpt.Station:Seibu.Ikebukuro.Tokorozawa", "所沢"),
    SEIBU_IKEBUKURO_NISHITOKOROZAWA("", "odpt.Station:Seibu.Ikebukuro.NishiTokorozawa", "西所沢"),
    SEIBU_IKEBUKURO_KOTESASHI("", "odpt.Station:Seibu.Ikebukuro.Kotesashi", "小手指"),
    SEIBU_IKEBUKURO_SAYAMAGAOKA("", "odpt.Station:Seibu.Ikebukuro.Sayamagaoka", "狭山ヶ丘"),
    SEIBU_IKEBUKURO_MUSASHIFUJISAWA("", "odpt.Station:Seibu.Ikebukuro.MusashiFujisawa", "武蔵藤沢"),
    SEIBU_IKEBUKURO_INARIYAMAKOEN("", "odpt.Station:Seibu.Ikebukuro.InariyamaKoen", "稲荷山公園"),
    SEIBU_IKEBUKURO_IRUMASHI("", "odpt.Station:Seibu.Ikebukuro.Irumashi", "入間市"),
    SEIBU_IKEBUKURO_BUSHI("", "odpt.Station:Seibu.Ikebukuro.Bushi", "仏子"),
    SEIBU_IKEBUKURO_MOTOKAJI("", "odpt.Station:Seibu.Ikebukuro.Motokaji", "元加治"),
    SEIBU_IKEBUKURO_HANNO("", "odpt.Station:Seibu.Ikebukuro.Hanno", "飯能"),;

    private static final Map<String, Station> maps = new HashMap<>();

    static {
        for (Station station : values()) {
            maps.put(station.value, station);
        }
    }

    private String code;

    private String value;

    private String name;

    Station(final String code, final String value, final String name) {
        this.code = code;
        this.value = value;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {

        return value;
    }

    public String getName() {
        return name;
    }

    public static Station getStation(String value) {
        return maps.get(value);
    }
}
