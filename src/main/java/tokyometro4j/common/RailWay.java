package tokyometro4j.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum RailWay {

    GINZA("odpt.Railway:TokyoMetro.Ginza", "東京メトロ銀座線"),
    MARUNOUCHI("odpt.Railway:TokyoMetro.Marunouchi", "東京メトロ丸ノ内線"),
    HIBIYA("odpt.Railway:TokyoMetro.Hibiya", "東京メトロ日比谷線"),
    TOZAI("odpt.Railway:TokyoMetro.Tozai", "東京メトロ東西線"),
    CHIYODA("odpt.Railway:TokyoMetro.Chiyoda", "東京メトロ千代田線"),
    YURAKUCHO("odpt.Railway:TokyoMetro.Yurakucho", "東京メトロ有楽町線"),
    HANZOMON("odpt.Railway:TokyoMetro.Hanzomon", "東京メトロ半蔵門線"),
    NAMBOKU("odpt.Railway:TokyoMetro.Namboku", "東京メトロ南北線"),
    FUKUTOSHIN("odpt.Railway:TokyoMetro.Fukutoshin", "東京メトロ副都心線"),
    OEDO("odpt.Railway:Toei.Oedo", "都営大江戸線"),
    JR_EAST("odpt.Railway:JR-East", "JR東日本")
    ;

    private static final Map<String, RailWay> maps = new HashMap<>();

    static {
        for (RailWay railWay : values()) {
            maps.put(railWay.value, railWay);
        }
    }

    private final String value;

    private final String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    RailWay(final String value, final String name) {
        this.value = value;
        this.name = name;
    }

    public static RailWay getRailWay(String value) {
        return maps.get(value);
    }
}
