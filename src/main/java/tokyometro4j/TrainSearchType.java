package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.RailDirection;
import tokyometro4j.common.RailWay;
import tokyometro4j.common.Station;
import tokyometro4j.common.TrainOwner;
import tokyometro4j.common.TrainType;
import tokyometro4j.entity.Train;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class TrainSearchType extends SearchType {

    protected final SearchApi searchApi;
    protected final String type = "odpt:Train";
    private List<NameValuePair> params = new ArrayList<>();

    public TrainSearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    public TrainSearchType station(Station station) {
        this.params.add(new BasicNameValuePair("odpt:station", station.getValue()));
        return this;
    }

    //    @id	URN	固有識別子(ucode)
    public TrainSearchType id(String id) {
        this.params.add(new BasicNameValuePair("@id", id));
        return this;
    }

    //    owl:sameAs	URL	固有識別子。命名ルールは、odpt.Train:TokyoMetro.路線名.列車番号である。
    public TrainSearchType sameAs(String train) {
        this.params.add(new BasicNameValuePair("odpt:station", train));
        return this;
    }

    //    odpt:trainNumber	xsd:string	列車番号
    public TrainSearchType trainNumber(String trainNumber) {
        this.params.add(new BasicNameValuePair("odpt:trainNumber", trainNumber));
        return this;
    }

    //    odpt:trainType	odpt:TrainType	列車種別。各停(odpt.TrainType:Local)、急行(odpt.TrainType:Express)、快速(odpt.TrainType:Rapid)、特急(odpt.TrainType:LimitedExpress)など。
    public TrainSearchType trainType(TrainType trainType) {
        this.params.add(new BasicNameValuePair("odpt:trainType", trainType.getValue()));
        return this;
    }

    //    odpt:railway	odpt:Railway	鉄道路線	◯
    public TrainSearchType railway(RailWay railway) {
        this.params.add(new BasicNameValuePair("odpt:railway", railway.getValue()));
        return this;
    }

    //    odpt:trainOwner	odpt:TrainOwner	車両の所属会社
    public TrainSearchType trainOwner(TrainOwner trainOwner) {
        this.params.add(new BasicNameValuePair("odpt:trainOwner", trainOwner.getValue()));
        return this;
    }

    //    odpt:railDirection	odpt:RailDirection	方面（渋谷方面行きodpt.RailDirection:TokyoMetro.Shibuyaなど。）
    public TrainSearchType railDirection(RailDirection railDirection) {
        this.params.add(new BasicNameValuePair("odpt:trainOwner", railDirection.getValue()));
        return this;
    }

    //    odpt:delay	xsd:integer	遅延時間（秒）
    public TrainSearchType delay(Integer delay) {
        this.params.add(new BasicNameValuePair("odpt:delay", String.valueOf(delay)));
        return this;
    }

    //    odpt:startingStation	odpt:Station	列車の始発駅
    public TrainSearchType startingStation(Station startingStation) {
        this.params.add(new BasicNameValuePair("odpt:startingStation", startingStation.getValue()));
        return this;
    }

    //    odpt:terminalStation	odpt:Station	列車の終着駅
    public TrainSearchType terminalStation(Station terminalStation) {
        this.params.add(new BasicNameValuePair("odpt:terminalStation", terminalStation.getValue()));
        return this;
    }

    //    odpt:fromStation	odpt:Station	列車が出発した駅
    public TrainSearchType fromStation(Station fromStation) {
        this.params.add(new BasicNameValuePair("odpt:fromStation", fromStation.getValue()));
        return this;
    }

    //    odpt:toStation	odpt:Station	列車が向かっている駅
    public TrainSearchType toStation(Station toStation) {
        this.params.add(new BasicNameValuePair("odpt:toStation", toStation.getValue()));
        return this;
    }


    public List<Train> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type).addParameters(params)
                .build();

        List<Train> trains = new ArrayList<>();
        List<Map<String, Object>> list = getApiResponse(uri);

        for (Map<String, Object> map : list) {
            trains.add(Train.parse(map));
        }

        return trains;
    }
}
