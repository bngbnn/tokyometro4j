package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.Operator;
import tokyometro4j.entity.Railway;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class DatapointsRailwaySearchType extends SearchType {

    protected final SearchApi searchApi;

    protected final String type = "odpt:Railway";

    protected List<NameValuePair> params = new ArrayList<>();

    public DatapointsRailwaySearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    //    @id	URN	固有識別子(ucode)
    public DatapointsRailwaySearchType id(String id) {
        this.params.add(new BasicNameValuePair("@id", id));
        return this;
    }

    //    owl:sameAs	URL	固有識別子。命名ルールは、odpt.Train:TokyoMetro.路線名.列車番号である。
    public DatapointsRailwaySearchType sameAs(String train) {
        this.params.add(new BasicNameValuePair("owl:sameAs", train));
        return this;
    }

    //     dc:title	xsd:string	駅名
    public DatapointsRailwaySearchType title(String title) {
        this.params.add(new BasicNameValuePair("dc:title", title));
        return this;
    }

    //     odpt:operator	odpt:Operator	運行会社
    public DatapointsRailwaySearchType operator(Operator operator) {
        this.params.add(new BasicNameValuePair("odpt:operator", operator.getValue()));
        return this;
    }

    //     odpt:lineCode	xsd:string	路線コード
    public DatapointsRailwaySearchType lineCode(String lineCode) {
        this.params.add(new BasicNameValuePair("odpt:lineCode", lineCode));
        return this;
    }

    public List<Railway> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type)
                .addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<Railway> railway = new ArrayList<>();

        for (Map<String, Object> map : list) {
            railway.add(Railway.parse(map));
        }

        return railway;
    }
}
