package tokyometro4j;

import org.apache.http.client.utils.URIBuilder;
import tokyometro4j.util.HttpUtil;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 *
 */
public abstract class SearchApi {

    protected TokyoMetro tokyoMetro;

    public abstract URIBuilder createUriBuilder() throws URISyntaxException;

    public abstract String getPath();

    /**
     * ucode で検索します。
     *
     * @param ucode ucode
     * @return 検索結果
     * @throws URISyntaxException  URL不正
     * @throws java.io.IOException ネットワークエラーなど
     */
    public SearchResponse id(String ucode) throws URISyntaxException, IOException {
        URIBuilder uriBuilder = tokyoMetro.createUriBuilder();
        uriBuilder.setPath(uriBuilder.getPath() + getPath() + "/" + ucode);
        List<Map<String, Object>> responses = HttpUtil.getApiResponse(uriBuilder.build());
        if (responses.isEmpty()) {
            return null;
        }
        return SearchResponse.parse(responses.get(0));
    }
}
