package tokyometro4j;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;

/**
 *
 */
public class DatapointsSearch extends SearchApi {

    protected String path = "datapoints";

    public DatapointsSearch(TokyoMetro tokyoMetro) {
        this.tokyoMetro = tokyoMetro;
    }

    public TrainSearchType train() {
        return new TrainSearchType(this);
    }

    public StationTimetableSearchType stationTimetable() {
        return new StationTimetableSearchType(this);
    }

    public DatapointsStationSearchType station() {
        return new DatapointsStationSearchType(this);
    }

    public DatapointsRailwaySearchType railway() {
        return new DatapointsRailwaySearchType(this);
    }

    /**
     * 列車運行情報の検索をします
     *
     * @return TrainInformationSearchType
     */
    public TrainInformationSearchType trainInformation() {
        return new TrainInformationSearchType(this);
    }

    /**
     * 駅乗降人員数の検索をします
     *
     * @return PassengerSurveySearchType
     */
    public PassengerSurveySearchType passengerSurvey() {
        return new PassengerSurveySearchType(this);
    }

    /**
     * 駅施設情報の検索をします
     *
     * @return StationFacilitySearchType
     */
    public StationFacilitySearchType stationFacility() {
        return new StationFacilitySearchType(this);
    }

    /**
     * 運賃の検索をします
     *
     * @return RailwayFareSearchType
     */
    public RailwayFareSearchType railwayFare() {
        return new RailwayFareSearchType(this);
    }

    @Override
    public URIBuilder createUriBuilder() throws URISyntaxException {
        URIBuilder uriBuilder = tokyoMetro.createUriBuilder();
        return uriBuilder.setPath(uriBuilder.getPath() + path);
    }

    @Override
    public String getPath() {
        return path;
    }
}
