package tokyometro4j;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import tokyometro4j.common.Station;
import tokyometro4j.entity.StationTimetable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class StationTimetableSearchType extends SearchType {

    protected final SearchApi searchApi;

    protected final String type = "odpt:StationTimetable";

    private List<NameValuePair> params = new ArrayList<>();

    public StationTimetableSearchType(final SearchApi searchApi) {
        this.searchApi = searchApi;
    }

    public StationTimetableSearchType station(Station station) {
        this.params.add(new BasicNameValuePair("odpt:station", station.getValue()));
        return this;
    }

    public List<StationTimetable> execute() throws URISyntaxException, IOException {

        URI uri = searchApi.createUriBuilder()
                .addParameter("rdf:type", this.type).addParameters(params)
                .build();

        List<Map<String, Object>> list = getApiResponse(uri);
        List<StationTimetable> stationTimetables = new ArrayList<>();

        for (Map<String, Object> map : list) {
            stationTimetables.add(StationTimetable.parse(map));
        }

        return stationTimetables;

    }

}
